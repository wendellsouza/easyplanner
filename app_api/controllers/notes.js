var mongoose = require('mongoose');
var EventModel = mongoose.model('Event');
var User = mongoose.model('User');

var sendJSONresponse = function(res, status, content) {
    res.status(status);
    res.json(content);
};

var getUser = function(req, res, callback) {
    if (req.payload && req.payload.email) {
        User
            .findOne({ email: req.payload.email })
            .exec(function(err, user) {
                if(!user) {
                    sendJSONResponse(res, 404, {
                        'message': 'Usuário não encontrado.'
                    });
                    return;
                } else if (err) {
                    console.log(err);
                    sendJSONResponse(res, 404, err);
                    return;
                }
                callback(req, res, user._id);
            });
    } else {
        sendJSONResponse(res, 404, {
            'message': 'Usuário não encontrado.'
        });
    }
};

/** POST a new note, but must provided with a eventid*/
/** path:'/events/:eventid/notes' */
module.exports.notesCreate = function (req, res ) {
    if (req.params.eventid) {
        getUser(req, res, function(req, res, user_id) {
            EventModel
                .findById(req.params.eventid)
                .where({user_id: user_id})
                .select('notes')
                .exec(
                    function (err, event) {
                        if (err) {
                            sendJSONresponse(res, 404, err);
                        } else {
                            doAddNotes(req, res, event);
                        }
                    }
                );
        });
    } else {
        sendJSONresponse(res, 404, {
            'message':'Não encontrado, eventid obrigatório.'
        });
    }
};

var doAddNotes = function (req, res, event) {
    if (!event) {
        sendJSONresponse(res, 404, 'eventid não encontrado.');
    } else {
        event.notes.push({
            note  : req.body.note,
            title : req.body.title
        });
        event.save(function(err, event){
            var thisNote;
            if (err) {
                console.log(err);
                sendJSONresponse(res, 404, err);
            } else {
                thisNote = event.notes[event.notes.length -1];
                sendJSONresponse(res, 201, thisNote);
            }
        });
    }
};

/** path: '/api/events/:eventid/notes/:noteid' */
module.exports.notesUpdateOne = function (req, res ) {
    var thisNote;
    if (!req.params.eventid || !req.params.noteid) {
        sendJSONresponse(res, 404, {
            'message': 'Não encontrado, eventid e noteid são obrigatórios'
        });
        return;
    }
    
    getUser(req, res, function(req, res, user_id) {
        EventModel
            .findById(req.params.eventid)
            .where({user_id: user_id})
            .select('notes')
            .exec(
                function (err, event) {
                    if (!event) {
                        sendJSONresponse(res, 404, {
                            'message': 'eventid não encontrado'
                        });
                        return;
                    } else if (err) {
                        sendJSONresponse(res, 400, err);
                        return;
                    }
                    if (event.notes && event.notes.length > 0) {
                        thisNote = event.notes.id(req.params.noteid);
                        if (!thisNote) {
                            sendJSONresponse(res, 404, {
                                'message': 'noteid not found'
                            });
                        } else {
                            thisNote.note  = req.body.note;
                            thisNote.title = req.body.title;
                            event.save(function (err) {
                                if (err) {
                                    sendJSONresponse(res, 404, err);
                                } else {
                                    sendJSONresponse(res, 200, thisNote);
                                }
                            });
                        }
                    } else {
                        sendJSONresponse(res, 404, {
                            'message': 'Nenhuma nota para atualizar'
                        });
                    }
                }
            );
    });
};

/** path: '/api/events/:eventid/notes/:noteid' */
module.exports.notesDeleteOne = function(req, res) {
    if (!req.params.eventid || !req.params.noteid) {
        sendJSONresponse(res, 404, {
            'message': 'Não encontrado, eventid e noteid são obrigatórios'
        });
        return;
    }
    
    getUser(req, res, function(req, res, user_id) {
        EventModel
            .findById(req.params.eventid)
            .where({user_id: user_id})
            .select('notes')
            .exec(
                function (err, event) {
                    if (!event) {
                        sendJSONresponse(res, 404, {
                            'message': 'eventid não encontrado'
                        });
                        return;
                    } else if (err) {
                        sendJSONresponse(res, 400, err);
                        return;
                    }
                    if (event.notes && event.notes.length > 0) {
                        if (!event.notes.id(req.params.noteid)) {
                            sendJSONresponse(res, 404, {
                                'message': 'noteid not found'
                            });
                        } else {
                            event.notes.id(req.params.noteid).remove();
                            event.save(function (err) {
                                if (err) {
                                    sendJSONresponse(res, 404, err);
                                } else {
                                    sendJSONresponse(res, 204, null);
                                }
                            });
                        }
                    } else {
                        sendJSONresponse(res, 404, {
                            'message': 'Nenhuma nota para excluir'
                        });
                    }
                }
            );
    });
};

/** Receive a file as atachment to the note */
/** path: '/events/:eventid/notes/upload/:noteid' */
module.exports.notesUpload = function(req, res) {
    var thisNote;
    var fs = require('fs');
    var Gridfs = require('gridfs-stream');
    
    var db = mongoose.connection.db;
    var mongoDriver = mongoose.mongo;
    var gfs = new Gridfs(db, mongoDriver);
    
    if (!req.params.eventid || !req.params.noteid) {
        sendJSONresponse(res, 404, {   'message': 'Não encontrado, eventid e noteid são obrigatórios' });
        return;
    }
    
    var writestream = gfs.createWriteStream({
        filename: req.files.file.name,
        mode: 'w',
        content_type: req.files.file.type,
        metadata: req.body
    });
    
    fs.createReadStream(req.files.file.path).pipe(writestream);
    
    writestream.on('close', function(file) {
        getUser(req, res, function (req, res, user_id) {
            EventModel
                .findById(req.params.eventid)
                .where({user_id: user_id})
                .select('notes')
                .exec(
                    function (err, event) {
                        if (!event) {
                            sendJSONresponse(res, 404, {
                                'message': 'eventid não encontrado'
                            });
                            return;
                        } else if (err) {
                            sendJSONresponse(res, 400, err);
                            return;
                        }
                        if (event.notes && event.notes.length > 0) {
                            thisNote = event.notes.id(req.params.noteid);
                            if (!thisNote) {
                                sendJSONresponse(res, 404, {
                                    'message': 'noteid not found'
                                });
                            } else {
                                
                                thisNote.fileId = file._id; // associa o Id do arquivo à nota
                                thisNote.fileName = file.filename; // associa o nome do arquivo à nota
                                
                                event.save(function (err) {
                                    if (err) {
                                        sendJSONresponse(res, 404, err);
                                    } else {
                                        fs.unlink(req.files.file.path, function (err) {
                                            if (err) {
                                                sendJSONresponse(res, 404, err);
                                                console.log(err);
                                            } else {
                                                sendJSONresponse(res, 204, null);
                                            }
                                        });
                                    }
                                });
                                
                            }
                        } else {
                            sendJSONresponse(res, 404, {
                                'message': 'Nenhuma nota para excluir'
                            });
                        }
                    }
                );
        });
    });
};

/** Gives a file atached to the note */
/** path: '/events/:eventid/notes/download/:noteid' */
module.exports.notesDownload = function(req, res) {
    var thisNote;
    var fs = require('fs');
    var Gridfs = require('gridfs-stream');
    
    var db = mongoose.connection.db;
    var mongoDriver = mongoose.mongo;
    var gfs = new Gridfs(db, mongoDriver);
    
    if (!req.params.eventid || !req.params.noteid) {
        sendJSONresponse(res, 404, {
            'message': 'Não encontrado, eventid e noteid são obrigatórios'
        });
        return;
    }
    getUser(req, res, function (req, res, user_id) {
        EventModel
            .findById(req.params.eventid)
            .where({user_id: user_id})
            .select('notes')
            .exec(
                function (err, event) {
                    if (!event) {
                        sendJSONresponse(res, 404, {
                            'message': 'eventid não encontrado'
                        });
                        return;
                    } else if (err) {
                        sendJSONresponse(res, 400, err);
                        return;
                    }
                    if (event.notes && event.notes.length > 0) {
                        thisNote = event.notes.id(req.params.noteid);
                        if (!thisNote) {
                            sendJSONresponse(res, 404, { 'message': 'noteid not found' });
                        } else {
                            gfs.findOne({ _id: thisNote.fileId }, function (err, file) {
                                if (err) {
                                    sendJSONresponse(res, 400, err);
                                    console.log(err);
                                    return;
                                }
                                else if (!file) {
                                    sendJSONresponse(res, 404, {'message': 'Error on the database looking for the file.'} );
                                    console.log('Erro ao fazer downalod de Notas. Arquivo não encontrado no Banco de Dados. Note fileId:' + thisNote.fileId);
                                    return;
                                }
        
                                res.set('Content-Type', file.contentType);
                                res.set('Content-Disposition', 'attachment; filename="' + file.filename + '"');
        
                                var readstream = gfs.createReadStream({
                                    _id: thisNote.fileId
                                });
        
                                readstream.on('error', function(err) {
                                    console.log('Erro ao fazer downalod de Notas - Note fileId: ' + thisNote.fileId + ' - Erro: ');
                                    console.log(err);
                                    sendJSONresponse(res, 400, {
                                        'message' : 'Erro ao fazer downalod de Notas - Note fileId: ' + thisNote.fileId ,
                                        'error': err });
                                });
                                readstream.pipe(res);
                            });
                        }
                    } else {
                        sendJSONresponse(res, 404, { 'message': 'Nota não encontrada.' });
                    }
                }
            );
    });
};