const stripe = require('stripe')(process.env.STRIPE_SECRET_API_KEY);
const stripe_plan_assinatura_49 = process.env.STRIPE_PLAN_ASSINATURA_49;

module.exports.createCustomer = (user) => {
    return new Promise(( resolve , reject) => {
        stripe
            .customers
            .create({
                email:  user.email,
                source: user.token.id,
            })
            .then(customer => {
                resolve(customer);
            })
            .catch(err => {
                console.error('Stripe error: ', err);
                getProperError(err, (err) => {
                    reject(err);
                });
            })
    });
};

module.exports.updateCard = (user) => {
    return new Promise(( resolve , reject) => {
        stripe
            .customers
            .update( user.customerId, {
                source: user.token.id,
            })
            .then(customer => {
                resolve(customer);
            })
            .catch(err => {
                console.error('Stripe error: ', err);
                getProperError(err, (err) => {
                    reject(err);
                });
            })
    });
};

module.exports.subscription = (stripeCustomer) => {
    return new Promise((resolve, reject) => {
        stripe
            .subscriptions
            .create({
                customer: stripeCustomer.id,
                items: [{plan: stripe_plan_assinatura_49}],
            })
            .then(subscription => {
                resolve(subscription);
            })
            .catch(err => {
                console.error('Stripe error: ', err);
                getProperError(err, (err) => {
                    reject(err);
                });
            });
    });
};

const retrieveCustomer = (stripeCustomer) => {
    return new Promise(function (resolve, reject) {
        stripe
            .customers
            .retrieve(stripeCustomer.id)
            .then(customer => {
                resolve(customer);
            })
            .catch(err => {
                reject(err);
            });
    })
};

module.exports.customerHasActiveSubscription = (stripeCustomer) => {
    return new Promise(function (resolve, reject) {
        retrieveCustomer(stripeCustomer)
            .then(function(customer) {
                if (customer.subscriptions.data.length > 0) {
                    customer.subscriptions.data.forEach(function(subscription) {
                        if(subscription.plan.active){
                            resolve(subscription);
                        }
                    });
                } else {
                    resolve(undefined);
                }
            })
            .catch(function (err) {
                reject(err);
            });
    })
};

module.exports.retrieveInvoiceByCustomer = (customer) => {
    return new Promise((resolve, reject) => {
        stripe
            .invoices
            .list({
                customer: customer.id
            })
            .then(invoices => {
                resolve(invoices);
            })
            .catch(err => {
                console.error(err);
                reject(err);
            })
    });
};

const getProperError = function(err, callback){
    err.easyplannerErr = {};
    switch (err.type) {
        case 'StripeCardError':
            // A declined card error
            err.easyplannerErr.message = 'Não autorizado pela operadora do cartão. Contate sua operadora do cartão para mais informações.';
            // if(err.message = 'Your card is not supported for this currency.') {
            //     err.easyplannerErr.message = 'Seu cartão não está habilitado para essa moeda.';
            // }
            // pegar decline code para melhorar a mensagem.
            break;
        case 'StripeInvalidRequestError':
            // Invalid parameters were supplied to Stripe's API
            err.easyplannerErr.message = 'Invalid parameters were supplied to Stripe API.';
            break;
        case 'StripeAPIError':
            // An error occurred internally with Stripe's API
            err.easyplannerErr.message = 'An error occurred internally with Stripe API.';
            break;
        case 'StripeConnectionError':
            // Some kind of error occurred during the HTTPS communication
            err.easyplannerErr.message = 'Some kind of error occurred during the HTTPS communication.';
            break;
        case 'StripeAuthenticationError':
            // You probably used an incorrect API key
            err.easyplannerErr.message = 'You probably used an incorrect API key.';
            break;
        case 'StripeRateLimitError':
            // Too many requests hit the API too quickly
            err.easyplannerErr.message = 'Too many requests hit the API too quickly.';
            break;
        default:
            err.easyplannerErr.message = 'Erro no Stripe.';
            break;
    }
    callback(err);
};

const getDeclineCode = function () {
    // approve_with_id	The payment cannot be authorized.	The payment should be attempted again. If it still cannot be processed, the customer needs to contact their card issuer.
    //     call_issuer	The card has been declined for an unknown reason.	The customer needs to contact their card issuer for more information.
    //     card_not_supported	The card does not support this type of purchase.	The customer needs to contact their card issuer to make sure their card can be used to make this type of purchase.
    //     card_velocity_exceeded	The customer has exceeded the balance or credit limit available on their card.	The customer should contact their card issuer for more information.
    //     currency_not_supported	The card does not support the specified currency.	The customer needs to check with the issuer whether the card can be used for the type of currency specified.
    //     do_not_honor	The card has been declined for an unknown reason.	The customer needs to contact their card issuer for more information.
    //     do_not_try_again	The card has been declined for an unknown reason.	The customer should contact their card issuer for more information.
    //     duplicate_transaction	A transaction with identical amount and credit card information was submitted very recently.	Check to see if a recent payment already exists.
    //     expired_card	The card has expired.	The customer should use another card.
    //     fraudulent	The payment has been declined as Stripe suspects it is fraudulent.	Do not report more detailed information to your customer. Instead, present as you would the generic_decline described below.
    //     generic_decline	The card has been declined for an unknown reason.	The customer needs to contact their card issuer for more information.
    //     incorrect_number	The card number is incorrect.	The customer should try again using the correct card number.
    //     incorrect_cvc	The CVC number is incorrect.	The customer should try again using the correct CVC.
    //     incorrect_pin	The PIN entered is incorrect. This decline code only applies to payments made with a card reader.	The customer should try again using the correct PIN.
    //     incorrect_zip	The ZIP/postal code is incorrect.	The customer should try again using the correct billing ZIP/postal code.
    //     insufficient_funds	The card has insufficient funds to complete the purchase.	The customer should use an alternative payment method.
    //     invalid_account	The card, or account the card is connected to, is invalid.	The customer needs to contact their card issuer to check that the card is working correctly.
    //     invalid_amount	The payment amount is invalid, or exceeds the amount that is allowed.	If the amount appears to be correct, the customer needs to check with their card issuer that they can make purchases of that amount.
    //     invalid_cvc	The CVC number is incorrect.	The customer should try again using the correct CVC.
    //     invalid_expiry_year	The expiration year invalid.	The customer should try again using the correct expiration date.
    //     invalid_number	The card number is incorrect.	The customer should try again using the correct card number.
    //     invalid_pin	The PIN entered is incorrect. This decline code only applies to payments made with a card reader.	The customer should try again using the correct PIN.
    //     issuer_not_available	The card issuer could not be reached, so the payment could not be authorized.	The payment should be attempted again. If it still cannot be processed, the customer needs to contact their card issuer.
    //     lost_card	The payment has been declined because the card is reported lost.	The specific reason for the decline should not be reported to the customer. Instead, it needs to be presented as a generic decline.
    //     merchant_blacklist	The payment has been declined because it matches a value on the Stripe user's blocklist.	Do not report more detailed information to your customer. Instead, present as you would the generic_decline described above.
    //     new_account_information_available	The card, or account the card is connected to, is invalid.	The customer needs to contact their card issuer for more information.
    //     no_action_taken	The card has been declined for an unknown reason.	The customer should contact their card issuer for more information.
    //     not_permitted	The payment is not permitted.	The customer needs to contact their card issuer for more information.
    //     pickup_card	The card cannot be used to make this payment (it is possible it has been reported lost or stolen).	The customer needs to contact their card issuer for more information.
    //     pin_try_exceeded	The allowable number of PIN tries has been exceeded.	The customer must use another card or method of payment.
    //     processing_error	An error occurred while processing the card.	The payment should be attempted again. If it still cannot be processed, try again later.
    //     reenter_transaction	The payment could not be processed by the issuer for an unknown reason.	The payment should be attempted again. If it still cannot be processed, the customer needs to contact their card issuer.
    //     restricted_card	The card cannot be used to make this payment (it is possible it has been reported lost or stolen).	The customer needs to contact their card issuer for more information.
    //     revocation_of_all_authorizations	The card has been declined for an unknown reason.	The customer should contact their card issuer for more information.
    //     revocation_of_authorization	The card has been declined for an unknown reason.	The customer should contact their card issuer for more information.
    //     security_violation	The card has been declined for an unknown reason.	The customer needs to contact their card issuer for more information.
    //     service_not_allowed	The card has been declined for an unknown reason.	The customer should contact their card issuer for more information.
    //     stolen_card	The payment has been declined because the card is reported stolen.	The specific reason for the decline should not be reported to the customer. Instead, it needs to be presented as a generic decline.
    //     stop_payment_order	The card has been declined for an unknown reason.	The customer should contact their card issuer for more information.
    //     testmode_decline	A Stripe test card number was used.	A genuine card must be used to make a payment.
    //     transaction_not_allowed	The card has been declined for an unknown reason.	The customer needs to contact their card issuer for more information.
    //     try_again_later	The card has been declined for an unknown reason.	Ask the customer to attempt the payment again. If subsequent payments are declined, the customer should contact their card issuer for more information.
    //     withdrawal_count_limit_exceeded	The customer has exceeded the balance or credit limit available on their card.	The customer should use an alternative payment method.
};