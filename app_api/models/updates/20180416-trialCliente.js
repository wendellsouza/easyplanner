/**
 * Created by wendell on 16/04/18.
 * branch: master
 * Atualiza todos os cliente para trial = true
 */
db.users.updateMany({}, { $set: { trial: true} });