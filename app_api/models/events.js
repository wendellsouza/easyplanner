const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
require('./user.js');

const Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

const fileSchema = new mongoose.Schema({
    fileId: ObjectId,
    fileName: String
});

const venueSchema = new mongoose.Schema({
    name: String,
    address: String,
    city: String,
    state: String,
    zipCode: String
});

const episodeSchema = new mongoose.Schema({
    name: String,
    hour: String,
    notes: String
});

const scriptSchema = new mongoose.Schema({
    name: String,
    episodes: [ episodeSchema ]
});

const taskSchema = new mongoose.Schema({
    name: String,
    done: Boolean,
    dueDate: Date,
    info: String
});

const providerSchema = new mongoose.Schema({
    type: String,
    name: String,
    email: String,
    phone: String,
    amount: Number,
    details: String,
    files: [ fileSchema ],
    contracted: { type: Boolean, default: false }
});

const customerSchema = new mongoose.Schema({
    name: String,
    lastName: String,
    role: String,
    email: String,
    birthDay: Date,
    phone: String,
    info: String
});

const messageSchema = new mongoose.Schema({
    message: String,
    date: Date,
    customer: { type: customerSchema }
});

const noteSchema = new mongoose.Schema({
    note: String,
    title: String,
    fileId: ObjectId,
    fileName: String
});

const guestSchema = new mongoose.Schema({
    name: String,
    email: String,
    phone: String,
    tableNumber: String,
    notes: String,
    qtyAdults: { type: Number, default: 0 },
    qtyKids: { type: Number, default: 0 },
    qtyFree: { type: Number, default: 0 },
    qtyAdultsConfirmed: { type: Number, default: 0 },
    qtyKidsConfirmed: { type: Number, default: 0 },
    qtyFreeConfirmed: { type: Number, default: 0 },
    confirmed: { type: Boolean, default: false }
});

/** TODO: criar um middleware para criar o mainCustomers */

const eventSchema = new mongoose.Schema({
    user_id:  { type: ObjectId, required: true },
    created:  Date,
    updated:  Date,
    type:     { type: String, required: true  },
    date:     { type: Date, required: true  },
    archived: { type: Boolean, required: false, default: false },
    info:          String,
    budget:        Number,
    qtdGuests:     Number,
    mainCustomers: String,
    accessCode:    { type: String, sparse: true, unique: true},  // 4 dígitos -> 1.679.616 / 5 dígitos-> 60.466.176 / 6 dígitos-> 2.176.782.336
    venues:    [ venueSchema ],
    scripts:   [ scriptSchema ],
    tasks:     [ taskSchema ],
    providers: [ providerSchema ],
    customers: [ customerSchema ],
    messages:  [ messageSchema ],
    notes:     [ noteSchema ],
    guests:    [ guestSchema ]
},  { usePushEach: true });

/**
 * Sum the total quantity of guests
 */
const setTotalGuest = (guests) => {
    let total = 0;
    guests.forEach(function (guest) {
        total += guest.qtyAdults + guest.qtyKids + guest.qtyFree;
    });
    return total;
};

/**
 * Pre-save middleware
 * */
eventSchema.pre('save', function(next) {
    const now = new Date();
    // Calculates sum fo guests if updating guests
    if (typeof this.guests  !== 'undefined') {
        this.qtdGuests = setTotalGuest(this.guests);
    }

    // Sets creation and update date on the Event
    if (this.isNew) {
        this.created = now;
    } else {
        this.updated = now;
    }
    next();
});

mongoose.model('Event', eventSchema);
