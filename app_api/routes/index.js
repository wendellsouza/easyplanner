const express = require('express');
const router = express.Router();
const jwt = require('express-jwt');
const auth = jwt({
    secret : process.env.JWT_SECRET,
    userProperty: 'payload'
});

const ctrlApp     = require('../controllers/app');
const ctrlAuth    = require('../controllers/authentication');
const ctrlPayment = require('../controllers/payment.js');

/** General App routes */
router.post('/log',        auth, ctrlApp.appLog);
router.get ('/server/url', auth, ctrlApp.serverUrl); //TODO: mudar o nome para ChatServer? pois fica estranho configurar uma constiável para o servidor da aplicação
router.post('/sendemail',  auth, ctrlApp.sendGeneralEmail);

/** Authentication routes */
router.post('/user/register',       ctrlAuth.register);
router.post('/user/login',          ctrlAuth.login);
router.put ('/user/update',   auth, ctrlAuth.update);

/** Payment routes */
router.post('/payment/subscription',   auth, ctrlPayment.subscription);
router.get('/payment/history',         auth, ctrlPayment.getHistoryInvoices);
router.get('/payment/customer',        auth, ctrlPayment.getCustomer);
router.post('/payment/updatecard',     auth, ctrlPayment.updateCard);
router.get('/payment/getpaymenthubid', auth, ctrlPayment.getPaymentHubId);

module.exports = router;