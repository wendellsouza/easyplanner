var express = require('express');
var router = express.Router();
var jwt = require('express-jwt');
var auth = jwt({
    secret : process.env.JWT_SECRET,
    userProperty: 'payload'
});

var multiparty = require('connect-multiparty')();

var ctrlEvents    = require('../controllers/events');
var ctrlVenues    = require('../controllers/venues');
var ctrlScripts   = require('../controllers/scripts');
var ctrlTasks     = require('../controllers/tasks');
var ctrlProviders = require('../controllers/providers');
var ctrlCustomers = require('../controllers/customers');
var ctrlMessages  = require('../controllers/messages');
var ctrlNotes     = require('../controllers/notes');
var ctrlGuests    = require('../controllers/guests');


/** Events pages */
router.post  ('/events',                        auth, ctrlEvents.eventsCreate);
router.get   ('/events/:eventid',               auth, ctrlEvents.eventsReadOne);
router.get   ('/users/events',                  auth, ctrlEvents.eventsListByUser);
router.get   ('/users/eventsqtdy',              auth, ctrlEvents.eventsQtdyByUser);
router.get   ('/users/events/archived',         auth, ctrlEvents.eventsArchivedByUser);
router.put   ('/events/:eventid',               auth, ctrlEvents.eventsUpdateOne);
router.put   ('/events/:eventid/budgetupdate',  auth, ctrlEvents.eventsBudgetUpdate);
router.put   ('/events/:eventid/archive',       auth, ctrlEvents.eventsArchive);
router.delete('/events/:eventid',               auth, ctrlEvents.eventsDeleteOne);

/** Venues */
router.post  ('/events/:eventid/venues',            auth, ctrlVenues.venuesCreate);
router.post  ('/events/:eventid/venues/:venueid',   auth, ctrlVenues.venuesUpdateOne);
router.delete('/events/:eventid/venues/:venueid',   auth, ctrlVenues.venuesDeleteOne);

/** Scripts */
router.post  ('/events/:eventid/scripts',                               auth, ctrlScripts.scriptsCreate);
router.post  ('/events/:eventid/scripts/:scriptid',                     auth, ctrlScripts.scriptsUpdateOne);
router.delete('/events/:eventid/scripts/:scriptid',                     auth, ctrlScripts.scriptsDeleteOne);
router.post  ('/events/:eventid/scripts/:scriptid/episodes',            auth, ctrlScripts.scriptsAddEpisode);
router.post  ('/events/:eventid/scripts/:scriptid/episodes/:episodeid', auth, ctrlScripts.scriptsUpdateEpisode);
router.delete('/events/:eventid/scripts/:scriptid/episodes/:episodeid', auth, ctrlScripts.scriptsDeleteEpisode);

/** Tasks */
router.post  ('/events/:eventid/tasks',         auth, ctrlTasks.tasksCreate);
router.post  ('/events/:eventid/tasks/:taskid', auth, ctrlTasks.tasksUpdateOne);
router.delete('/events/:eventid/tasks/:taskid', auth, ctrlTasks.tasksDeleteOne);

/** Provider */
router.post  ('/events/:eventid/vendors',                                  auth, ctrlProviders.providersCreate);
router.post  ('/events/:eventid/vendors/:providerid',                      auth, ctrlProviders.providersUpdateOne);
router.delete('/events/:eventid/vendors/:providerid',                      auth, ctrlProviders.providersDeleteOne);
router.post  ('/events/:eventid/vendors/:providerid/uploadfile',           auth, ctrlProviders.providersUploadFile);
router.get   ('/events/:eventid/vendors/:providerid/downloadfile/:fileid', auth, ctrlProviders.providersDownloadFile);
router.delete('/events/:eventid/vendors/:providerid/deletefile/:fileid',   auth, ctrlProviders.providersDeleteFile);

/** Customer */
router.post  ('/events/:eventid/customers',             auth, ctrlCustomers.customersCreate);
router.post  ('/events/:eventid/customers/invite',      auth, ctrlCustomers.invite);
// router.get   ('/events/:eventid/customers/invite',      auth, ctrlCustomers.invite);
router.post  ('/events/:eventid/customers/:customerid', auth, ctrlCustomers.customersUpdateOne);
router.delete('/events/:eventid/customers/:customerid', auth, ctrlCustomers.customersDeleteOne);

/** Messages */
router.post  ('/events/:eventid/messages',              auth, ctrlMessages.messagesCreate);
router.post  ('/events/:eventid/messages/:messageid',   auth, ctrlMessages.messagesUpdateOne);
router.delete('/events/:eventid/messages/:messageid',   auth, ctrlMessages.messagesDeleteOne);

/** Notes */
router.post  ('/events/:eventid/notes',                              auth, ctrlNotes.notesCreate);
router.post  ('/events/:eventid/notes/:noteid',                      auth, ctrlNotes.notesUpdateOne);
router.delete('/events/:eventid/notes/:noteid',                      auth, ctrlNotes.notesDeleteOne);
router.post  ('/events/:eventid/notes/upload/:noteid',   multiparty, auth, ctrlNotes.notesUpload);
router.get   ('/events/:eventid/notes/download/:noteid',             auth, ctrlNotes.notesDownload);

/*** Guests */
    /** For Users */
router.post  ('/events/:eventid/guests',          auth, ctrlGuests.guestsCreate);
router.post  ('/events/:eventid/guests/:guestid', auth, ctrlGuests.guestsUpdateOne);
router.delete('/events/:eventid/guests/:guestid', auth, ctrlGuests.guestsDeleteOne);

    /** For Guests */
router.put   ('/events/rsvpGuest/:guestname',           ctrlGuests.guestsReadOneByName);
    /** For Customers */
router.get   ('/events/rsvpCustomer/:accesscode',       ctrlGuests.guestsReadByCode);
    /** For Guests */
router.post  ('/events/:eventid/guests/:guestid/rsvp',  ctrlGuests.guestsRsvp);

router.post  ('/guest/events/:eventid/guests',          ctrlGuests.guestsCreateCustomer);
router.post  ('/guest/events/:eventid/guests/:guestid', ctrlGuests.guestsUpdateOneCustomer);
router.delete('/guest/events/:eventid/guests/:guestid', ctrlGuests.guestsDeleteOneCustomer);

module.exports = router;