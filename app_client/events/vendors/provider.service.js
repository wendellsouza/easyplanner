/**
 * Created by wendell on 1/5/18.
 */
(function () {
    'use strict';

    function providerService (eventsService) {
        let _provider = {};

        this.setProvider = function (provider) {
            _provider = provider;
        };

        this.getProvider = function () {
            return _provider;
        };

        this.clearProvider = function  () {
            _provider = {};
        };

        this.getVendorsEmailList = function () {
            let eventInfo = eventsService.getActiveEvent(),
                emailList = [];
            eventInfo.providers.forEach(function (vendor) {
                if (vendor.email) {
                    emailList.push({
                        'name'  : vendor.name,
                        'email' : vendor.email });
                }
            });
            return emailList;
        };
    }

    angular
        .module('app.providers')
        .service('providerService', ['eventsService', providerService]);

})();