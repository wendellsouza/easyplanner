/**
 * Created by wendell on 12/5/17.
 */
(function () {
    'use strict';
    function VenuesController ($scope, $location, $uibModal, $timeout,eventsService, venueService, logger, globalModalService) {
        var vm = this;
        vm.eventInfo = eventsService.getActiveEvent();
        $scope.isCollapsed = true;
        vm.addAddressString = function () {
            for (var key in vm.eventInfo.venues) {
                /** Create the string for use in Google Maps search */
                vm.eventInfo.venues[key].addressString =
                    ( !vm.eventInfo.venues[key].name    ? '' :         vm.eventInfo.venues[key].name ) +
                    ( !vm.eventInfo.venues[key].address ? '' : ( '+' + vm.eventInfo.venues[key].address ) ) +
                    ( !vm.eventInfo.venues[key].city    ? '' : ( '+' + vm.eventInfo.venues[key].city ) ) +
                    ( !vm.eventInfo.venues[key].state   ? '' : ( '+' + vm.eventInfo.venues[key].state ) ) +
                    ( !vm.eventInfo.venues[key].zipCode ? '' : ( '+' + vm.eventInfo.venues[key].zipCode ) );
                /** Create the string show the complete address */
                vm.eventInfo.venues[key].completeAddress =
                    ( !vm.eventInfo.venues[key].address ? '' : ( vm.eventInfo.venues[key].address + ', ' ) ) +
                    ( !vm.eventInfo.venues[key].city    ? '' : ( vm.eventInfo.venues[key].city ) ) +
                    ( (!vm.eventInfo.venues[key].state  || !vm.eventInfo.venues[key].city) ? '' : '/' ) +
                    ( !vm.eventInfo.venues[key].state   ? '' : vm.eventInfo.venues[key].state );
            }
        };
        vm.addAddressString();
        /** Adding */
        vm.venueAdd = function () {
            $scope.isCollapsed = !$scope.isCollapsed;
            $timeout(function () {angular.element('#venueName').focus(); }, 100);
        };
        /** Update */
        vm.venueEdit = function (venue) {
            venueService.setVenue(venue);
            $location.path('/updateVenue');
            $timeout(function () {angular.element('#venueName').focus(); }, 100);
        };
    
        /** Delete */
        vm.venueDelete = function (venue) {
            var modalInstance = $uibModal.open( globalModalService.getUibModalObj('core/modal/okCancelModal/okCancelModal.html', 'OkCancelModalCtrl', 'Excluir Local', 'Confirma a exclusão deste Local?' ) );
            modalInstance.result.then(function () {
                venueService.deleteEventVenue(venue)
                    .then( function() {
                        vm.eventInfo = eventsService.getActiveEvent();
                        logger.logSuccess('Local apagado com sucesso.'); })
                    .catch(function (err) {
                        logger.logError('Erro ao apagar Local.');
                        console.error(err);
                    });
            });
        };
    
        $scope.$on('$editVenueCancelled', function () { $scope.isCollapsed = true; });
        $scope.$on('$editVenueAdded', function () {  $scope.isCollapsed = true;  });
        $scope.$on('$activeEventUpdated', function () {  vm.eventInfo = eventsService.getActiveEvent(); vm.addAddressString(); });
    }
    
    function EditVenueController($scope, $location, venueService, eventsService, logger) {
        var vm = this;
        vm.back = function () { $location.path('/venues'); };
        vm.editVenue = venueService.getVenue();
        vm.states = eventsService.STATES;
        
        if (angular.equals(vm.editVenue, {}) ) { vm.back(); } /** Se não houver venue em memória volta pra lista */
        
        vm.cleanObj = function () {
            venueService.clearVenue();
            vm.editVenue = {};
        };
        vm.canSubmit = function() {
            return $scope.formEditVenue.$valid;
        };
        vm.cancel = function () {
            vm.cleanObj();
            $scope.$emit('$editVenueCancelled');
            vm.back();
        };
        vm.submitForm = function () {
            venueService
                .editEventVenue(vm.editVenue)
                .then( function() {
                    vm.cleanObj();
                    $scope.$emit('$editVenueAdded');
                    logger.logSuccess('Local Incluído/atualizado com sucesso.');
                    vm.back(); })
                .catch(function (err) {
                    logger.logError('Erro ao Incluir/editar Local.');
                    console.error(err);
                });
        };
    }
    
    angular
        .module('app.venues')
        .controller('VenuesController', ['$scope','$location', '$uibModal', '$timeout', 'eventsService', 'venueService', 'logger', 'globalModalService', VenuesController])
        .controller('EditVenueController', [ '$scope', '$location', 'venueService', 'eventsService', 'logger', EditVenueController]);
    
})();