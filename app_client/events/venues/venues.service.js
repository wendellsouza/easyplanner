/**
 * Created by wendell on 29/5/18.
 */
(function () {
    'use strict';
    
    function venueService ($http, eventsService, authentication) {
        var _venue = {};
        
        this.setVenue = function (venue) {
            _venue = venue;
        };
        
        this.getVenue = function () {
            return _venue;
        };
        
        this.clearVenue = function  () {
            _venue = {};
        };


        /** Venues */
        var editEventVenue = function (editingVenue) {
            return new Promise( function (resolve, reject) {
                var activeEvent, url;
                activeEvent = eventsService.getActiveEvent();
                url = editingVenue._id ?
                    '/api/events/' + activeEvent._id + '/venues/' + editingVenue._id :
                    '/api/events/' + activeEvent._id + '/venues';
                $http.post(url, editingVenue, {headers: { Authorization: 'Bearer ' + authentication.getEasyToken() } })
                    .then(function (newVenue) {
                        eventsService.eventAddObjById(activeEvent.venues, newVenue.data);
                        eventsService.setActiveEvent(activeEvent);
                        resolve();
                    })
                    .catch(function (err) {
                        reject(err.data);
                    });
            });
        };
        var deleteEventVenue = function (deletingVenue) {
            return new Promise(function (resolve, reject) {
                var activeEvent = eventsService.getActiveEvent();
                $http.delete('/api/events/' + activeEvent._id + '/locals/' + deletingVenue._id, { headers: { Authorization: 'Bearer ' + authentication.getEasyToken() } })
                    .then(function () {
                        eventsService.eventDeleteObjById(activeEvent.venues, deletingVenue);
                        eventsService.setActiveEvent(activeEvent);
                        resolve();
                    })
                    .catch(function (err) {
                        reject(err.data);
                    });
            });
        };

        return {
            setVenue : this.setVenue,
            getVenue : this.getVenue,
            clearVenue : this.clearVenue,

            editEventVenue : editEventVenue,
            deleteEventVenue : deleteEventVenue
        }
    }
    
    angular
        .module('app.venues')
        .service('venueService', [ '$http', 'eventsService', 'authentication', venueService]);
    
})();