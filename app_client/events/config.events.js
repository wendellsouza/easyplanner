/**
 * Created by wendell on 3/3/18.
 */
(function () {
    'use strict';

    angular
        .module('app.events')
        .config([ '$mdDateLocaleProvider', '$provide', function($mdDateLocaleProvider, $provide) {

            /** Date Config */
            moment.locale('pt-br');

            $mdDateLocaleProvider.parseDate = function(dateString) {
                var m = moment(dateString, 'L', true);
                return m.isValid() ? m.toDate() : new Date(NaN);
            };
            $mdDateLocaleProvider.formatDate = function(date) {
                var m = moment(date);
                return m.isValid() ? m.format('L') : '';
            };

            $mdDateLocaleProvider.months      = moment.months();
            $mdDateLocaleProvider.shortMonths = moment.monthsShort();
            $mdDateLocaleProvider.days        = moment.weekdays();
            $mdDateLocaleProvider.shortDays   = moment.weekdaysShort();

            /** textAngular Config */
            $provide.decorator('taOptions', ['taRegisterTool', '$delegate', function(taRegisterTool, taOptions) {
            //$delegate is the taOptions we are decorating
                taOptions.toolbar = [
                    // ['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'p', 'quote'],
                    ['bold', 'italics', 'underline', 'strikeThrough', 'ul', 'ol', 'redo', 'undo', 'clear'],
                    ['justifyLeft', 'justifyCenter', 'justifyRight', 'indent', 'outdent']
                ];
                return taOptions;
            }]);
        }]);
})();