/**
 * Created by wendell on 12/11/17.
 */
(function () {
    'use strict';

    angular
        .module('app.events', [
            // 3rd Party Modules
            'textAngular'

            // easyplanner
            ,'app.notes'
            ,'app.episodes'
            ,'app.scripts'
            ,'app.providers'
            ,'app.tasks'
            ,'app.customers'
            ,'app.venues'
            ,'app.guests'
        ]);
})();