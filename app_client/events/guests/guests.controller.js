/**
 * Created by wendell on 03/12/18.
 */

(function () {
    'use strict';
    function GuestsController ($scope, $uibModal, $log, $location, $anchorScroll, $timeout, eventsService, guestService, logger, globalModalService, customerService, authentication, emailModalService, tooltipModalRsvpService) {
        var vm = this;
        vm.eventInfo = eventsService.getActiveEvent();
        vm.isCustomer = function () {
            return !authentication.isLoggedIn() && authentication.isCustomerLogged();
        };

        vm.showTooltip = () => {
            tooltipModalRsvpService
                .tooltipModal()
                .then(function (answer) {
                    logger.logSuccess(answer);
                })
                .catch(function (error) {
                    if (error) {
                        console.error(error);
                    }
                });
        };


        function guestRefreshLocal() {
            vm.eventInfo = eventsService.getActiveEvent();
        }
        function guestRefresh() {
            if (vm.isCustomer()) {
                guestService
                    .readEventGuests(authentication.getEventCode())
                    .then( function(eventGuest) {
                        guestService.setGuest(eventGuest);
                        vm.sortByName();
                        $location.path('/updateRsvp');
                    })
                    .catch(function (err) {
                        logger.logError('Ooop\'s!  Aconteceu algum erro.');
                        console.error(err);
                    });
            } else {
                eventsService
                    .getEventInfo(vm.eventInfo._id)
                    .then(function () {
                        vm.eventInfo = eventsService.getActiveEvent();
                        vm.sortByName();
                    })
                    .catch(function () {
                        logger.logError('Erro ao autalizar o Evento.');
                    });
            }
        }

        /** Sorts RSVP */
        vm.sortByName = function () {
            vm.eventInfo.guests.sort( function (a, b) {
                if(!a.name || !b.name) {return 0;}
                return a.name.localeCompare(b.name);
            });
        };
        guestRefresh();

        $scope.addUpdateIsCollapsed = true;
        $scope.confirmIsCollapsed = true;

        $scope.$on('$editGuestCancelled', function () { $scope.addUpdateIsCollapsed = true; $scope.confirmIsCollapsed = true; goToAnchor(); }); // fecha tela
        $scope.$on('$editGuestUpdated',   function () { $scope.addUpdateIsCollapsed = true; $scope.confirmIsCollapsed = true; goToAnchor(); }); // fecha tela
        $scope.$on('$guestConfirmed',     function () { $scope.addUpdateIsCollapsed = true; $scope.confirmIsCollapsed = true; goToAnchor(); }); // fecha tela
        $scope.$on('$editGuestAdded',     function () { $timeout(function () {angular.element('#guestName').focus();}, 100);});                 // mantém tela de edição aberta pra facilitar inclusão
        $scope.$on('$activeEventUpdated', function () { guestRefresh(); });
        $scope.$on('$activeEventUpdatedLocally', function () { guestRefreshLocal(); });

        vm.filterGuests = function(guest) {
            if      (vm.filter === 'confirmed') { return guest.confirmed }
            else if (vm.filter === 'pending')   { return !guest.confirmed }
        };

        function goToAnchor() {
            var _hash = guestService.getGuest()._id;
            $scope.addUpdateIsCollapsed = true;
            $scope.confirmIsCollapsed = true;
            if ($location.hash() !== _hash) {
                $location.hash(_hash);
            } else {
                $anchorScroll();
            }
        }
        /** Adding */
        vm.guestAdd = function () {
            $scope.addUpdateIsCollapsed = false;
            $scope.$broadcast('$addGuestStarted');
            $timeout(function () {angular.element('#guestName').focus(); }, 100);
        };
        /** Editing */
        vm.guestEdit = function (guest) {
            guestService.setGuest(Object.assign({}, guest));
            $scope.addUpdateIsCollapsed = false;
            $anchorScroll.yOffset = 555;
            $scope.$broadcast('$editGuestStarted');
            $timeout(function () { angular.element('#guestName').focus(); }, 100);
        };
        /** Confirming */
        vm.guestConfirm = function (guest) {
            guestService.setGuest(Object.assign({}, guest));
            $scope.confirmIsCollapsed = false;
            $anchorScroll.yOffset = 470;
            // $scope.$broadcast('$editGuestStarted');
            $scope.$broadcast('$confirming');
            $timeout(function () { angular.element('#qtyAdultsConfirmed').focus(); }, 100);
        };
        /** Deleting */
        vm.guestDelete = function (guest) {
            var modalInstance = $uibModal.open( globalModalService.getUibModalObj('core/modal/okCancelModal/okCancelModal.html', 'OkCancelModalCtrl', 'Excluir Convidado', 'Confirma a exclusão deste Convidado?' ) );
            modalInstance.result.then(function () {
                guestService.deleteEventGuest(guest)
                    .then( function() {
                        logger.logSuccess('Convidado apagado com sucesso.'); })
                    .catch(function (err) {
                        logger.logError('Erro ao apagar Convidado.');
                        console.error(err);
                    });
            });
        };
        /** Inviting */
        vm.customerInvite = function () {
            var modalInstance = $uibModal.open( globalModalService
                .getUibModalObj('core/modal/okCancelModal/okCancelModal.html',
                    'OkCancelModalCtrl',
                    'Convidar Cliente',
                    'Confirma enviar e-mail para convidar os Clientes para o RSVP?' ) );
            modalInstance.result.then(function () {
                customerService
                    .inviteEventCustomers()
                    .then( function() {
                        logger.logSuccess('Cliente convidado com sucesso.');
                    })
                    .catch(function (err) {
                        if ( err.message === 'Sem email cadastrado.') {
                            var msg = '<div><a href="#events/customers/customers">Clientes sem e-mail cadastrado. <strong><i>Clique aqui para resolver.</i></strong></a></div>';
                            logger.logError(msg);
                        } else {
                            logger.logError('Erro ao convidar Cliente.');
                            console.error(err);
                        }
                    });
            });
        };

        /** Send E-mail */
        $scope.customFullscreen = false; // TODO: ?

        vm.sendEmail =  function (guest) {
            if (!guest.email) {
                logger.logWarning('Cadastre o e-mail para compartilhar algo com o Convidado.');
                return false;
            }

            emailModalService
                .emailModal(
                    'E-mail para ' + guest.name,
                    '',
                    guest.email,
                    ''
                )
                .then(function (answer) {
                    logger.logSuccess(answer);
                })
                .catch(function (error) {
                    if (error) {
                        logger.logError('Erro ao enviar e-mail.');
                        console.error(error);
                    }
                });

        }
    }

    function EditGuestController($scope, $location, $timeout, guestService, logger, eventsService) {
        var vm = this;
        vm.editing = false;
        vm.confirming = false;
        vm.newGuestMessage = '';
        vm.editGuest = guestService.getGuest();
        vm.event = eventsService.getActiveEvent();
        $scope.$on('$editGuestStarted', function() {
            vm.editGuest = guestService.getGuest();
            vm.newGuestMessage = '';
            vm.editing = true;
            $timeout(function () {
                angular.element('#qtyAdultsConfirmed').focus();
            })
        });

        $scope.$on('$addGuestStarted', function() {
            vm.cleanObj();
        });

        $scope.$on('$confirming', function () {
            vm.editGuest = guestService.getGuest();
            vm.editGuest.confirmed = true;
            vm.confirming = true;
            $timeout(function () {
                angular.element('#qtyAdultsConfirmed').focus();
            })
        });

        vm.cleanObj = function () {
            vm.editGuest = {};
            vm.newGuestMessage = '';
        };
        vm.canSubmit = function() {
            return vm.formEditGuest.$valid;
        };
        vm.cancel = function () {
            vm.cleanObj();
            $scope.$emit('$editGuestCancelled');
        };
        vm.submitForm = function () {
            /** Não permite número negativo */
            if (vm.editGuest.qtyAdultsConfirmed < 0 || vm.editGuest.qtyAdults < 0 ||
                vm.editGuest.qtyKidsConfirmed   < 0 || vm.editGuest.qtyKids   < 0 ||
                vm.editGuest.qtyFreeConfirmed   < 0 || vm.editGuest.qtyFree   < 0 ) {
                logger.logError('Quantidade deve ser maior que zero.');
                return;
            }
            /** Verifica se N confirmação não ultrapassa o N convidado */
            if (vm.editGuest.qtyAdultsConfirmed > vm.editGuest.qtyAdults ||
                vm.editGuest.qtyKidsConfirmed   > vm.editGuest.qtyKids   ||
                vm.editGuest.qtyFreeConfirmed   > vm.editGuest.qtyFree ) {
                logger.logError('Quantidade confirmada não pode ser maior que a convidada.');
                return;
            }
            if (vm.confirming) {
                guestService
                    .rsvpGuestConfirm(vm.editGuest)
                    .then(function () {
                        $scope.messageCollapsed = false;
                        $scope.confirmCollapsed = true;
                        $scope.$emit('$guestConfirmed');
                        logger.logSuccess('Convidado confirmado com sucesso.');
                    })
                    .catch(function (err) {
                        logger.logError('Oops! Aconteceu algum problema. Tente novamente ou avisa aos Anfitriões.');
                        console.error(err);
                    });

                vm.confirming = false;
            } else {
                guestService
                    .editEventGuest(vm.editGuest)
                    .then( function() {
                        vm.cleanObj();
                        vm.editing ? (
                            $scope.$emit('$editGuestUpdated'),
                                vm.editing = false
                        ): (
                            $scope.$emit('$editGuestAdded'),
                                vm.newGuestMessage = 'Convidado incluído! Inclua outro convidado:'
                        );
                        logger.logSuccess('Convidado Incluído/atualizado com sucesso.');

                    })
                    .catch(function (err) {
                        logger.logError('Erro ao Incluir/editar Convidado.');
                        console.error(err);
                    });
            }
        };
    }

    function GuestAccessController ($scope, $location, $timeout, guestService) {
        var vm = this;
        $timeout(function () {angular.element('#eventDate').focus(); }, 100);
        document.title = "Sistema RSVP";
        vm.canSubmit = function() {
            return vm.formGuestAccess.$valid;
        };
        vm.submitForm = function () {
            vm.guestConfirmed = false;
            vm.notFound = false;
            vm.error = false;
            guestService
                .readOneEventGuest(vm.guestName, {eventDate: vm.eventDate})
                .then( function(eventGuest) {
                    guestService.setGuest(eventGuest.guests[0]);
                    if (eventGuest.confirmed) {
                        vm.guestConfirmed = true;
                    } else {
                        $location.path('/confirmGuest');
                    }
                })
                .catch(function (err) {
                    if (err.message === 'Evento não encontrado.') {
                        vm.notFound = true;
                    } else {
                        vm.error = true;
                        console.error(err);
                    }
                });
        };
    }

    function CustomerAccessController ($scope, $location, $timeout, $stateParams, guestService, logger) {
        var vm = this;
        $timeout(function () {angular.element('#eventCode').focus(); }, 100);
        vm.canSubmit = function() {
            return vm.formCustomerAccess.$valid;
        };

        vm.eventCode = $stateParams.accesscode;

        if (vm.eventCode) {
            guestService
                .readEventGuests(vm.eventCode)
                .then( function(eventGuest) {
                    guestService.setGuest(eventGuest);
                    $location.path('/updateRsvp');
                })
                .catch(function (err) {
                    logger.logError('Ooop\'s!  Aconteceu algum erro.');
                    console.error(err);
                });
        }
    }

    function RsvpController ($scope, $location, $timeout, guestService, eventsService) {
        var vm = this;
        $scope.$on('$guestConfirmed', function () { $scope.confirmCollapsed = true; });
        vm.guest = guestService.getGuest();
        vm.event = eventsService.getActiveEvent();

        /** Se não tiver guest salvo não deixa usar a página */
        if (angular.equals(vm.guest, {})) {
            $location.path('/rsvp');
            return;
        }
        $timeout(function () {
            $scope.$broadcast('$confirming');
        });
    }

    angular
        .module('app.guests')
        .controller('GuestsController', ['$scope', '$uibModal', '$log', '$location', '$anchorScroll', '$timeout', 'eventsService', 'guestService', 'logger', 'globalModalService', 'customerService', 'authentication', 'emailModalService', 'tooltipModalRsvpService', GuestsController])
        .run(['$anchorScroll', function($anchorScroll) {
            $anchorScroll.yOffset = 210; // pixels extras por causa do cabeçalho
        }])
        .controller('GuestAccessController', [ '$scope', '$location', '$timeout', 'guestService', GuestAccessController])
        .controller('CustomerAccessController', [ '$scope', '$location', '$timeout', '$stateParams', 'guestService', 'logger', CustomerAccessController])
        .controller('RsvpController', [ '$scope', '$location', '$timeout', 'guestService', 'eventsService', RsvpController])
        .controller('EditGuestController', [ '$scope', '$location', '$timeout', 'guestService', 'logger', 'eventsService', EditGuestController]);
})();