/**
 * Created by wendell on 29/5/18.
 */
(function () {
    'use strict';

    function guestService (eventsService, authentication, $http) {
        var _guest = {};

        this.setGuest = function (guest) {
            _guest = guest;
        };

        this.getGuest = function () {
            return _guest;
        };

        this.clearGuest = function () {
            this.setGuest({});
        };

        var editEventGuest = function (editingGuest) {
            return new Promise( function (resolve, reject) {
                var activeEvent, url;
                activeEvent = eventsService.getActiveEvent();
                /** Customer */
                if (!authentication.isLoggedIn() && authentication.isCustomerLogged()) {
                    url = editingGuest._id ?
                        '/api/guest/events/' + activeEvent._id + '/guests/' + editingGuest._id :
                        '/api/guest/events/' + activeEvent._id + '/guests';

                    $http
                        .post(url, editingGuest)
                        .then(function () {
                            eventsService.setActiveEvent(activeEvent);
                            resolve();
                        })
                        .catch(function (err) {
                            reject(err.data);
                        });
                }
                /** User */
                else {
                    url = editingGuest._id ?
                        '/api/events/' + activeEvent._id + '/guests/' + editingGuest._id :
                        '/api/events/' + activeEvent._id + '/guests';

                    $http
                        .post(url, editingGuest,
                            {headers: { Authorization: 'Bearer ' + authentication.getEasyToken() } })
                        .then(function (eventUpdated) {
                            eventsService.setActiveEvent(eventUpdated.data);
                            resolve();
                        })
                        .catch(function (err) {
                            reject(err.data);
                        });
                }
            });
        };

        var deleteEventGuest = function (deletingGuest) {
            return new Promise(function (resolve, reject) {
                var activeEvent = eventsService.getActiveEvent();
                if (!authentication.isLoggedIn() && authentication.isCustomerLogged()) {
                    $http
                        .delete('/api/guest/events/' + activeEvent._id + '/guests/' + deletingGuest._id)
                        .then(function () {
                            eventsService.eventDeleteObjById(activeEvent.guests, deletingGuest);
                            eventsService.setActiveEvent(activeEvent);
                            resolve();
                        })
                        .catch(function (err) {
                            reject(err.data);
                        });
                } else {
                    $http
                        .delete('/api/events/' + activeEvent._id + '/guests/' + deletingGuest._id,
                            {headers: {Authorization: 'Bearer ' + authentication.getEasyToken()}})
                        .then(function () {
                            eventsService.eventDeleteObjById(activeEvent.guests, deletingGuest);
                            eventsService.setActiveEvent(activeEvent);
                            resolve();
                        })
                        .catch(function (err) {
                            reject(err.data);
                        });
                }
            });
        };

        /**
         * Exposed to the guests >>>
         * */
        var readOneEventGuest = function (guestName, eventDate) {
            var guest;
            return new Promise(function (resolve, reject) {
                $http
                    .put('/api/events/rsvpGuest/' + guestName, eventDate)
                    .then(function (data) {
                        if ( data.data.length > 1 ) {
                            reject({'message' : 'Consulta retornou mais de um evento.'});
                        } else {
                            guest = Object.assign(
                                { _id: data.data[0]._id}, // _id: ID do evento
                                {guests: [data.data[0].guests[0]]},
                                {mainCustomers: data.data[0].mainCustomers },
                                {type: data.data[0].type });
                            eventsService.setActiveEvent(guest);
                            resolve(guest);
                        }
                    })
                    .catch(function (err) {
                        console.error(err);
                        reject(err.data);
                    });
            });
        };
        var rsvpGuestConfirm = function (editingGuest) {
            var activeEvent = eventsService.getActiveEvent();
            return new Promise( function (resolve, reject) {
                var url;
                url = '/api/events/' + activeEvent._id + '/guests/' + editingGuest._id + '/rsvp';

                $http
                    .post(url, editingGuest)
                    .then(function (guestUpdated) {
                        eventsService.eventAddObjById(activeEvent.guests, guestUpdated.data);
                        eventsService.setActiveEvent(activeEvent);
                        resolve();
                    })
                    .catch(function (err) {
                        reject(err.data);
                    });
            });
        };
        /**
         * Exposed to the guests <<<
         * */

        /**
         * Exposed to Customers >>>
         * */
        var readEventGuests = function (accessCode) {
            var eventToLocalUpdate = '$activeEventUpdatedLocally';
            return new Promise(function (resolve, reject) {
                $http
                    .get('/api/events/rsvpCustomer/' + accessCode)
                    .then(function (event) {
                        authentication.setEventCode(accessCode);
                        eventsService.setActiveEvent(event.data[0], eventToLocalUpdate);
                        resolve(event);
                    })
                    .catch(function (err) {
                        reject(err.data);
                    });
            });
        };
        /**
         * Exposed to Customers <<<
         * */

        return {
            getGuest : this.getGuest,
            clearGuest : this.clearGuest,
            setGuest : this.setGuest,
            editEventGuest : editEventGuest,
            deleteEventGuest : deleteEventGuest,
            readOneEventGuest : readOneEventGuest,
            rsvpGuestConfirm : rsvpGuestConfirm,
            readEventGuests : readEventGuests
        }
    }

    angular
        .module('app.guests')
        .service('guestService', ['eventsService', 'authentication', '$http', guestService]);

})();