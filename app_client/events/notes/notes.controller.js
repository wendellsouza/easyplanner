/**
 * Created by wendell on 30/01/18.
 */

( function () {
    'use strict';
    function NotesController ($scope, $uibModal, $log, $location, $timeout, eventsService, noteService, logger, globalModalService) {
        var vm = this;
        vm.eventInfo = eventsService.getActiveEvent();
        $scope.isCollapsed = true;

        $scope.$on('$editNoteCancelled', function () { $scope.isCollapsed = true; });
        $scope.$on('$editNoteAdded', function () { $scope.isCollapsed = true;  });
        $scope.$on('$activeEventUpdated', function () { vm.eventInfo = eventsService.getActiveEvent(); });

        /** Adding */
        vm.noteAdd = function () {
            $scope.isCollapsed = !$scope.isCollapsed;
            $timeout(function () {angular.element('#noteTitle').focus(); }, 100);
        };
        /** Update */
        vm.noteEdit = function (note) {
            noteService.setNote(note);
            $location.path('/updateNote');
            $timeout(function () {angular.element('#noteTitle').focus(); }, 100);
        };

        /** Delete */
        vm.noteDelete = function (note) {
            var modalInstance = $uibModal
                .open(
                    globalModalService
                        .getUibModalObj('core/modal/okCancelModal/okCancelModal.html',
                            'OkCancelModalCtrl',
                            'Excluir Documento',
                            'Confirma a exclusão deste Documento?' )
                );
            modalInstance.result.then(function () {
                eventsService.deleteEventNote(note)
                    .then( function() {
                        vm.eventInfo = eventsService.getActiveEvent();
                        logger.logSuccess('Documento apagado com sucesso.'); })
                    .catch(function (err) {
                        logger.logError('Erro ao apagar Documento.');
                        console.error(err);
                    });
            });
        };
    }

    function EditNoteController($scope, $location, noteService, eventsService, logger, $http, authentication) {
        var vm = this;
        vm.back = function () { $location.path('/notes'); };
        vm.editNote = noteService.getNote();

        if (angular.equals(vm.editNote, {}) ) { vm.back(); } /** Se não houver note em memória volta pra lista */

        vm.cleanObj = function () {
            noteService.clearNote();
            vm.editNote = {};
        };
        vm.canSubmit = function() {
            return $scope.formEditNote.$valid;
        };
        vm.cancel = function () {
            vm.cleanObj();
            $scope.$emit('$editNoteCancelled');
            vm.back();
        };
        vm.submitForm = function () {
            eventsService.editEventNote(vm.editNote)
                .then( function() {
                    vm.cleanObj();
                    $scope.$emit('$editNoteAdded');
                    logger.logSuccess('Documento Incluído/atualizado com sucesso.');
                    vm.back(); })
                .catch(function (err) {
                    logger.logError('Erro ao Incluir/editar Documento.');
                    console.error(err);
                });
        };
    }

    angular
        .module('app.notes')
        .controller('NotesController', ['$scope', '$uibModal', '$log', '$location', '$timeout', 'eventsService', 'noteService', 'logger', 'globalModalService', NotesController])
        .controller('EditNoteController', [ '$scope', '$location', 'noteService', 'eventsService', 'logger', '$http', 'authentication',  EditNoteController]);
}) ();