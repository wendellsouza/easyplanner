/**
 * Created by wendell on 1/29/18.
 */
(function () {
    'use strict';

    function episodeService ($http, eventsService, authentication, scriptService) {
        var _episode = {};

        this.setEpisode = function (episode) {
            _episode = episode;
        };

        this.getEpisode = function () {
            return _episode;
        };

        this.clearEpisode = function  () {
            _episode = {};
        };

        var editEventEpisode = function (editingEpisode) {
            return new Promise( function (resolve, reject) {
                var activeEvent = eventsService.getActiveEvent(),
                    activeScript = scriptService.getScript(),
                    url = editingEpisode._id ?
                        '/api/events/' + activeEvent._id + '/scripts/' + activeScript._id + '/episodes/' + editingEpisode._id :
                        '/api/events/' + activeEvent._id + '/scripts/' + activeScript._id + '/episodes';
                $http.post(url, editingEpisode, {headers: { Authorization: 'Bearer ' + authentication.getEasyToken() } })
                    .then(function (newEpisode) {
                        var index = eventsService.getArrayIndexByObjId(activeEvent.scripts, activeScript);
                        eventsService.eventAddObjById(activeEvent.scripts[index].episodes, newEpisode.data);
                        eventsService.setActiveEvent(activeEvent);
                        scriptService.setScript(activeEvent.scripts[index]);
                        resolve();
                    })
                    .catch(function (err) {
                        reject(err.data);
                    });
            });
        };
        var deleteEventEpisode = function (deletingEpisode) {
            return new Promise(function (resolve, reject) {
                var activeEvent = eventsService.getActiveEvent(),
                    activeScript = scriptService.getScript();
                $http.delete('/api/events/' + activeEvent._id + '/scripts/' + activeScript._id + '/episodes/' + deletingEpisode._id,
                    { headers: { Authorization: 'Bearer ' + authentication.getEasyToken() } })
                    .then(function () {
                        var index = eventsService.getArrayIndexByObjId(activeEvent.scripts, activeScript);
                        eventsService.eventDeleteObjById(activeEvent.scripts[index].episodes, deletingEpisode);
                        eventsService.setActiveEvent(activeEvent);
                        scriptService.setScript(activeEvent.scripts[index]);
                        resolve();
                    })
                    .catch(function (err) {
                        reject(err.data);
                    });
            });
        };

        return {
            setEpisode : this.setEpisode,
            getEpisode : this.getEpisode,
            clearEpisode : this.clearEpisode,

            editEventEpisode : editEventEpisode,
            deleteEventEpisode : deleteEventEpisode
        }
    }

    angular
        .module('app.episodes')
        .service('episodeService', ['$http', 'eventsService', 'authentication', 'scriptService', episodeService]);

})();