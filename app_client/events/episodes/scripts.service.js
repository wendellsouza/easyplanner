/**
 * Created by wendell on 1/29/18.
 */
(function () {
    'use strict';

    function scriptService ($http, eventsService, authentication, $window) {
        var _script = {};
        const ACTIVE_SCRIPT = 'easyplanner.script'; // TODO: gravar o script no session storage

        this.setScript = function (script, index) {
            _script = script;
            if( index) {script.index = index;}
            $window.sessionStorage[ACTIVE_SCRIPT] = JSON.stringify(script);
        };
        
        var setActiveScript = this.setScript;

        this.getScript = function () {
            var script;
            script = $window.sessionStorage[ACTIVE_SCRIPT];
            if (!script) { return _script; }
            return JSON.parse( $window.sessionStorage[ACTIVE_SCRIPT]);
        };

        this.clearScript = function () {
            _script = {};
            $window.sessionStorage.removeItem(ACTIVE_SCRIPT);
        };

        var editEventScript = function (editingScript) {
            return new Promise( function (resolve, reject) {
                var activeEvent, url;
                activeEvent = eventsService.getActiveEvent();
                url = editingScript._id ?
                    '/api/events/' + activeEvent._id + '/scripts/' + editingScript._id :
                    '/api/events/' + activeEvent._id + '/scripts';
                $http.post(url, editingScript, {headers: { Authorization: 'Bearer ' + authentication.getEasyToken() } })
                    .then(function (newScript) {
                        eventsService.eventAddObjById(activeEvent.scripts, newScript.data);
                        eventsService.setActiveEvent(activeEvent);
                        setActiveScript(newScript.data);
                        resolve(newScript.data);
                    })
                    .catch(function (err) {
                        reject(err.data);
                    });
            });
        };
        var deleteEventScript = function (deletingScript) {
            return new Promise(function (resolve, reject) {
                var activeEvent = eventsService.getActiveEvent();
                $http.delete('/api/events/' + activeEvent._id + '/scripts/' + deletingScript._id, { headers: { Authorization: 'Bearer ' + authentication.getEasyToken() } })
                    .then(function () {
                        eventsService.eventDeleteObjById(activeEvent.scripts, deletingScript);
                        eventsService.setActiveEvent(activeEvent);
                        resolve();
                    })
                    .catch(function (err) {
                        reject(err.data);
                    });
            });
        };

        return {
            setScript : this.setScript,
            getScript : this.getScript,
            clearScript : this.clearScript,

            editEventScript : editEventScript,
            deleteEventScript : deleteEventScript
        }
    }
    
    angular
        .module('app.episodes')
        .service('scriptService', ['$http', 'eventsService', 'authentication', '$window', scriptService]);
    
})();