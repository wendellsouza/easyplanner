/**
 * Created by wendell on 12/11/17.
 */

( function() {
    'use strict';
    
    function eventsService ($http, $window, $rootScope, authentication) {
        /** Constants */
        const OTHER_TYPE = 'Outro';
        /** Messages */
        const MSG_EVENTO_NOT_FOUND_NOT_ALLOWED = 'Evento não encontrado ou sem acesso esse evento.';
        /** Events */
        const EVENT_TYPES = ['Casamento', 'Aniversário', 'Bodas', '15 Anos', 'Corporativo', 'Formatura'];
        const eventTypesObject = {
            CASAMENTO    : 'Casamento',
            ANIVERSARIO  : 'Aniversário',
            BODAS        : 'Bodas',
            QUINZEANOS   : '15 Anos',
            CORPORATIVO  : 'Corporativo',
            FORMATURA    : 'Formatura'
        };
        const EASYPLANNER_ACTIVE_EVENT = 'easyplanner-active-event'; // TODO: guardar esse nome de token em outro lugar.
        const STATES = ['AC', 'AL', 'AP', 'AM', 'BA', 'CE', 'DF', 'ES', 'GO', 'MA', 'MT', 'MS', 'MG', 'PA', 'PB', 'PR', 'PE', 'PI', 'RJ', 'RN', 'RS', 'RO', 'RR', 'SC', 'SP', 'SE', 'TO'];
        
        /** Funções para Array do evento */
        var eventDeleteObjById = function (array, obj) {
            var index = array.findIndex( function (a) { return a._id === obj._id; } );
            array.splice(index, 1);
        };
        var eventAddObjById = function (array, obj) {
            var index = getArrayIndexByObjId(array, obj);
            (index > -1) ?
                array[index] = obj :
                array.push(obj);
        };

        var getArrayIndexByObjId = function (array, obj) {
            return array.findIndex(function (a) { return a._id === obj._id; });
        };
            /**
         * Calcula os totais para RSVP
         * */
        function setTotals(event) {
            event.guestTotal = 0;
            event.guestConfirmedTotal = 0;

            event.adultsTotal = 0;
            event.kidsTotal = 0;
            event.freeTotal = 0;
            event.adultsConfirmed = 0;
            event.kidsConfirmed = 0;
            event.freeConfirmed = 0;

            if ( !event.guests ) { return; }
            event.guests.forEach( function(guest) {
                event.adultsTotal += guest.qtyAdults;
                event.kidsTotal   += guest.qtyKids;
                event.freeTotal   += guest.qtyFree;

                event.adultsConfirmed += guest.qtyAdultsConfirmed;
                event.kidsConfirmed += guest.qtyKidsConfirmed;
                event.freeConfirmed += guest.qtyFreeConfirmed;

                event.guestConfirmedTotal += guest.qtyAdultsConfirmed + guest.qtyKidsConfirmed + guest.qtyFreeConfirmed;
            });
            event.guestTotal = event.adultsTotal + event.kidsTotal + event.freeTotal;
        }
        
        /**  Guarda o objecto do Evento */
        var setActiveEvent = function (event, eventToLocalUpdate) {
            // TODO: encriptografar ou codificar.
            setTotals(event);
            $window.sessionStorage[EASYPLANNER_ACTIVE_EVENT] = JSON.stringify(event);
            if (eventToLocalUpdate) {
                $rootScope.$broadcast(eventToLocalUpdate);
            } else {
                $rootScope.$broadcast('$activeEventUpdated');
            }
        };
        
        var getActiveEvent = function () {
            var event;
            event = $window.sessionStorage[EASYPLANNER_ACTIVE_EVENT];
            if (!event) { return; }
            return JSON.parse($window.sessionStorage[EASYPLANNER_ACTIVE_EVENT]);
        };
        
        var addEvent = function (data) {
            return new Promise( function (resolve, reject) {
                $http.post('/api/events', data, {
                        headers: { Authorization: 'Bearer ' + authentication.getEasyToken() } })
                    .then(function (newEvent) {
                        setActiveEvent(newEvent.data);
                        resolve();
                    })
                    .catch(function (err) {
                        reject(err);
                    });
                
            });
        };
        
        var updateEvent = function (editingEvent) {
            return new Promise( function (resolve, reject) {
                var localEvent = getActiveEvent();
                $http
                    .put('/api/events/' + editingEvent._id, editingEvent, {
                        headers: { Authorization: 'Bearer ' + authentication.getEasyToken() } })
                    .then(function (updatedEvent) {
                        localEvent.date          = updatedEvent.data.date;
                        localEvent.qtdGuests     = updatedEvent.data.qtdGuests;
                        localEvent.budget        = updatedEvent.data.budget;
                        localEvent.mainCustomers = updatedEvent.data.mainCustomers;
                        setActiveEvent( localEvent );
                        resolve();
                    })
                    .catch(function (err) {
                        reject(err.data);
                    });
            });
        };
        
        var getEventInfo = function (eventId) {
            var eventToLocalUpdate = '$activeEventUpdatedLocally';
            return new Promise( function (resolve, reject) {
                $http
                    .get('/api/events/' + eventId, {
                        headers: { Authorization: 'Bearer ' + authentication.getEasyToken() }
                    })
                    .then(function (event) {
                        setActiveEvent(event.data, eventToLocalUpdate);
                        resolve();
                    })
                    .catch( function (err) {
                        reject(err.data);
                    });
            })
        };
        
        var updateEventBudget = function (newBudget) {
            return new Promise( function (resolve, reject) {
                var activeEvent = getActiveEvent();
                $http.put('/api/events/' + activeEvent._id + '/budgetupdate', { budget: newBudget }, {
                        headers: { Authorization: 'Bearer ' + authentication.getEasyToken() } })
                    .then(function (updatedEvent) {
                        activeEvent.budget = updatedEvent.data.budget;
                        setActiveEvent(activeEvent);
                        resolve();
                    })
                    .catch(function (err) {
                        reject(err.data);
                    });
            });
        };
        
        var archiveEvent = function (eventId, data) {
            return new Promise( function (resolve, reject) {
                $http.put('/api/events/' + eventId + '/archive', data, { headers: { Authorization: 'Bearer ' + authentication.getEasyToken() } })
                    .then(function (updatedEvent) {
                        $window.localStorage.removeItem(EASYPLANNER_ACTIVE_EVENT);
                        resolve(updatedEvent.data);
                    })
                    .catch(function (err) {
                        reject(err.data);
                    });
                
            });
        };

        var deleteEvent = function (eventId) {
            return new Promise( function (resolve, reject) {
                $http.delete('/api/events/' + eventId, { headers: { Authorization: 'Bearer ' + authentication.getEasyToken() } })
                    .then(function (updatedEvent) {
                        resolve(updatedEvent.data);
                    })
                    .catch(function (err) {
                        reject(err.data);
                    });

            });
        };

        var getQtdyUserEvents = function () {
            return new Promise( function (resolve, reject) {
                $http.get('/api/users/eventsqtdy/',
                    { headers: { Authorization: 'Bearer ' + authentication.getEasyToken() } })
                    .then(function (qtdy) {
                        console.info(qtdy)
                        resolve(qtdy.data);
                    })
                    .catch(function (err) {
                        reject(err.data);
                    });

            });
        };


        

        
        /** Tasks */
        var editEventTask = function (editingTask) {
            return new Promise( function (resolve, reject) {
                var activeEvent, url;
                activeEvent = getActiveEvent();
                url = editingTask._id ?
                    '/api/events/' + activeEvent._id + '/tasks/' + editingTask._id :
                    '/api/events/' + activeEvent._id + '/tasks';
                $http.post(url, editingTask, {headers: { Authorization: 'Bearer ' + authentication.getEasyToken() } })
                    .then(function (newTask) {
                        eventAddObjById(activeEvent.tasks, newTask.data);
                        setActiveEvent(activeEvent);
                        resolve();
                    })
                    .catch(function (err) {
                        reject(err.data);
                    });
            });
        };
        var deleteEventTask = function (deletingTask) {
            return new Promise(function (resolve, reject) {
                var activeEvent = getActiveEvent();
                $http.delete('/api/events/' + activeEvent._id + '/tasks/' + deletingTask._id, { headers: { Authorization: 'Bearer ' + authentication.getEasyToken() } })
                    .then(function () {
                        eventDeleteObjById(activeEvent.tasks, deletingTask);
                        setActiveEvent(activeEvent);
                        resolve();
                    })
                    .catch(function (err) {
                        reject(err.data);
                    });
            });
        };
        
        /** Providers */
        const providersTypes = ['Acessórios & Joias','Assessoria & Cerimonial','Banda','Bares & Bebidas','Beleza & Dia da Noiva','Bolo','Buffet e Bebidas',
            'Celebrantes','Convites & Papelaria','Coral & Orquestra','Daminhas & Pajens','Decoração','Detalhes Especiais','DJ, Som & Iluminação',
            'Doces','Espaço de Eventos','Filmagem','Florista','Fotografia','Geradores Elétricos','Lembrancinha','Lista de Presentes','Lua de Mel',
            'Móveis & Materiais','Noite de Núpcias','Roupa do Noivo','Site de Casamento','Vestido de Noiva'];
        
        var editEventProvider = function (editingProvider) {
            return new Promise( function (resolve, reject) {
                var activeEvent, url;
                activeEvent = getActiveEvent();
                url = editingProvider._id ?
                    '/api/events/' + activeEvent._id + '/vendors/' + editingProvider._id :
                    '/api/events/' + activeEvent._id + '/vendors';
                $http.post(url, editingProvider, {headers: { Authorization: 'Bearer ' + authentication.getEasyToken() } })
                    .then(function (newProvider) {
                        eventAddObjById(activeEvent.providers, newProvider.data);
                        setActiveEvent(activeEvent);
                        resolve();
                    })
                    .catch(function (err) {
                        reject(err.data);
                    });
            });
        };
        var deleteEventProvider = function (deletingProvider) {
            return new Promise(function (resolve, reject) {
                var activeEvent = getActiveEvent();
                $http.delete('/api/events/' + activeEvent._id + '/vendors/' + deletingProvider._id, { headers: { Authorization: 'Bearer ' + authentication.getEasyToken() } })
                    .then(function () {
                        eventDeleteObjById(activeEvent.providers, deletingProvider);
                        setActiveEvent(activeEvent);
                        resolve();
                    })
                    .catch(function (err) {
                        reject(err.data);
                    });
            });
        };
        var uploadProviderFile = function (editingProvider, file) {
            return new Promise( function (resolve, reject) {
                var activeEvent = getActiveEvent();
                $http({
                    method: 'POST',
                    url: '/api/events/' + activeEvent._id + '/vendors/' + editingProvider._id + '/uploadfile',
                    data: { 'file': file },
                    headers: { Authorization: 'Bearer ' + authentication.getEasyToken(),
                              'Content-Type': undefined },
                    transformRequest: function (data) {
                        var formData = new FormData();
                        angular.forEach(data, function (value, key) {
                            formData.append(key, value);
                        });
                        return formData;
                    }})
                    .then(function (newFileObject) {
                        eventAddObjById(editingProvider.files, newFileObject.data);
                        eventAddObjById(activeEvent.providers, editingProvider);
                        setActiveEvent(activeEvent);
                        resolve();
                    })
                    .catch(function (err) {
                        reject(err.data);
                    });
            });
        };
        var downloadProviderFile = function (editingProvider, file) {
            return new Promise( function (resolve, reject) {
                var activeEvent = getActiveEvent();
                $http({
                    method: 'GET',
                    url: '/api/events/' + activeEvent._id + '/vendors/' + editingProvider._id + '/downloadfile/' + file._id,
                    headers: { Authorization: 'Bearer ' + authentication.getEasyToken()},
                    responseType: 'arraybuffer'
                }).success(function (data, status, headers) {
                    headers = headers();
                    var filename = file.fileName;
                    var contentType = headers['content-type'];
    
                    var linkElement = document.createElement('a');
                    try {
                        var blob = new Blob([data], { type: contentType });
                        var url = window.URL.createObjectURL(blob);
        
                        linkElement.setAttribute('href', url);
                        linkElement.setAttribute('download', filename);
        
                        var clickEvent = new MouseEvent('click', {
                            'view': window,
                            'bubbles': true,
                            'cancelable': false
                        });
                        linkElement.dispatchEvent(clickEvent);
                    } catch (err) {
                        console.log(err);
                    }
                    resolve();
                }).error(function (err) {
                    reject(err.data);
                });
            });
        };
        var deleteProviderFile = function (editingProvider, file) {
            return new Promise( function (resolve, reject) {
                var activeEvent = getActiveEvent();
                $http({
                    method: 'DELETE',
                    url: '/api/events/' + activeEvent._id + '/vendors/' + editingProvider._id + '/deletefile/' + file._id,
                    headers: { Authorization: 'Bearer ' + authentication.getEasyToken()}})
                    .then(function () {
                        eventDeleteObjById(editingProvider.files, file);
                        eventAddObjById(activeEvent.providers, editingProvider);
                        setActiveEvent(activeEvent);
                        resolve();
                    })
                    .catch(function (err) {
                        reject(err.data);
                    });
            });
        };

        /** Note */
        var editEventNote = function (note) {
            return new Promise( function (resolve, reject) {
                var event = getActiveEvent();
                var url = note._id ?
                    '/api/events/' + event._id + '/notes/' + note._id :     /** Update */
                    '/api/events/' + event._id + '/notes/';                 /** Add */
                $http.post(url, note, { headers: { Authorization: 'Bearer ' + authentication.getEasyToken() } })
                    .then(function (newNote) {
                        eventAddObjById(event.notes, newNote.data);
                        setActiveEvent(event);
                        resolve();
                    })
                    .catch(function (err) {
                        reject(err.data);
                    });
            });
        };
        var deleteEventNote = function (note) {
            return new Promise( function(resolve, reject) {
                var event = getActiveEvent();
                $http.delete('/api/events/' + event._id + '/notes/' + note._id, { headers: { Authorization: 'Bearer ' + authentication.getEasyToken() } })
                    .then(function () {
                        eventDeleteObjById(event.notes, note);
                        setActiveEvent(event);
                        resolve();
                    })
                    .catch(function (err) {
                        reject(err.data);
                    });
            });
        };
        
        
        return {
            EVENT_TYPES : EVENT_TYPES,
            eventTypesObject : eventTypesObject,
            OTHER_TYPE : OTHER_TYPE,
            STATES : STATES,

            eventDeleteObjById : eventDeleteObjById,
            eventAddObjById : eventAddObjById,
            getArrayIndexByObjId : getArrayIndexByObjId,

            addEvent : addEvent,
            updateEvent : updateEvent,
            getEventInfo : getEventInfo,
            updateEventBudget : updateEventBudget,
            archiveEvent : archiveEvent,
            deleteEvent : deleteEvent,
            getQtdyUserEvents : getQtdyUserEvents,

            setActiveEvent : setActiveEvent,
            getActiveEvent : getActiveEvent,
            
            // deleteEventEpisode : deleteEventEpisode,
            // editEventEpisode : editEventEpisode,
            
            editEventTask : editEventTask,
            deleteEventTask : deleteEventTask,
            
            editEventProvider : editEventProvider,
            deleteEventProvider : deleteEventProvider,
            uploadProviderFile : uploadProviderFile,
            downloadProviderFile : downloadProviderFile,
            deleteProviderFile : deleteProviderFile,
            providersTypes : providersTypes,
            
            editEventNote : editEventNote,
            deleteEventNote : deleteEventNote,
            
            
            /** Menssages */
            MSG_EVENTO_NOT_FOUND_NOT_ALLOWED: MSG_EVENTO_NOT_FOUND_NOT_ALLOWED
        };
    }
    
    angular
        .module('app.events')
        .service('eventsService', [ '$http', '$window', '$rootScope', 'authentication', eventsService ]);
    
}) ();