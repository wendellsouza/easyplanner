(function () {
    'use strict';

    angular
        .module('app')
        .config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

            /** Usuário */
            $stateProvider.state('dashboard', {
                url: '/dashboard',
                templateUrl: 'user/dashboard/dashboard.html',
            });
            $stateProvider.state('profile', {
                url: '/profile',
                templateUrl: 'user/profile/profile.html',
            });
            $stateProvider.state('signin', {
                url: '/signin',
                templateUrl: 'user/authentication/signin.html',
            });
            $stateProvider.state('signup', {
                url: '/signup',
                templateUrl: 'user/authentication/signup.html',
            });
            /** Eventos */
            $stateProvider.state('notes', {
                url: '/notes',
                templateUrl: 'events/notes/notes.html',
            });
            $stateProvider.state('updateNote', {
                url: '/updateNote',
                templateUrl: 'events/notes/updateNote.html',
            });
            $stateProvider.state('scripts', {
                url: '/scripts',
                templateUrl: 'events/episodes/scripts.html',
            });
            $stateProvider.state('episodes', {
                url: '/episodes',
                templateUrl: 'events/episodes/episodes.html',
            });
            $stateProvider.state('updateEpisode', {
                url: '/updateEpisode',
                templateUrl: 'events/episodes/updateEpisode.html',
            });
            $stateProvider.state('vendors', {
                url: '/vendors',
                templateUrl: 'events/vendors/providers.html',
            });
            $stateProvider.state('updateVendor', {
                url: '/updateVendor',
                templateUrl: 'events/vendors/updateProvider.html',
            });
            $stateProvider.state('tasks', {
                url: '/tasks',
                templateUrl: 'events/tasks/tasks.html',
            });
            $stateProvider.state('updateTask', {
                url: '/updateTask',
                templateUrl: 'events/tasks/updateTask.html',
            });
            $stateProvider.state('customers', {
                url: '/customers',
                templateUrl: 'events/customers/customers.html',
            });
            $stateProvider.state('updateCustomer', {
                url: '/updateCustomer',
                templateUrl: 'events/customers/updateCustomer.html',
            });
            $stateProvider.state('venues', {
                url: '/venues',
                templateUrl: 'events/venues/venues.html',
            });
            $stateProvider.state('updateVenue', {
                url: '/updateVenue',
                templateUrl: 'events/venues/updateVenue.html',
            });
            $stateProvider.state('guests', {
                url: '/guests',
                templateUrl: 'events/guests/guests.html'
            });


            /** Noivos*/
            $stateProvider.state('cutomerGuest', {
                url: '/rsvpnoivos/:accesscode',
                templateUrl: 'events/guests/rsvpNoivos.html'
            });
            $stateProvider.state('updateRsvp', {
                url: '/updateRsvp',
                templateUrl: 'events/guests/customerGuest.html'
            });


            /** Convidados*/
            $stateProvider.state('rsvp', {
                url: '/rsvp',
                templateUrl: 'events/guests/rsvp.html'
            });
            $stateProvider.state('confirmationGuest', {
                url: '/confirmGuest',
                templateUrl: 'events/guests/confirmationGuests.html'
            });

            $urlRouterProvider
                .when('/', '/signin')
                .otherwise('/signin');

        }]
    );
})();
