(function () {
    'use strict';

    angular
        .module('app')
        .config(['$compileProvider', '$trace', '$transitions', function ($compileProvider, $trace, $transitions) {

            /** For disable Angular debug info produtction */
            $compileProvider.debugInfoEnabled(false);

            $trace.enable('TRANSITION');

            $transitions.onStart({ }, function(trans) {
                trans.promise.finally();
            });
        }])
})();