/**
 * Created by wendell on 8/27/18.
 */

(function () {
    'use strict';

    function emailModalService ($mdDialog) {

        var emailModal = function(title, subject, mailTo, mailText, html2Pdf) {

            return new Promise (function (resolve, reject) {
                $mdDialog.show({
                    controller: 'EmailModalController',
                    templateUrl: 'core/modal/emailModal/emailModal.html',
                    parent: angular.element(document.body),
                    // targetEvent: ev,
                    clickOutsideToClose: true,
                    fullscreen: false, // Only for -xs, -sm breakpoints.
                    resolve: {
                        title:    function () { return title; },
                        subject:  function () { return subject; },
                        mailTo:   function () { return mailTo; },
                        mailText: function () { return mailText; },
                        html2Pdf: function () { return html2Pdf; }
                    }
                }).then(function(answer) {
                    resolve(answer);
                }, function(error) {
                    reject(error);
                });
            })
        };

        return {
            emailModal : emailModal
        }
    }

    angular
        .module('app')
        .service('emailModalService', [ '$mdDialog', emailModalService])
})();