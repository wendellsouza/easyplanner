(function () {
    'use strict';

    angular.module('app.page')
        .directive('customPage', customPage);


    // add class for specific pages to achieve fullscreen, custom background etc.
    function customPage() {
        var directive = {
            restrict: 'A',
            controller: ['$scope', '$element', '$location', customPageCtrl]
        };

        return directive;

        function customPageCtrl($scope, $element, $location) {
            var addBg, path;

            path = function() {
                return $location.path();
            };

            addBg = function(path) {
                $element.removeClass('body-wide body-wide-sign body-err body-lock body-auth');
                switch (path) {
                    case '/404':
                    case '/pages/404': // TODO: do the page. =)
                    case '/pages/500': // TODO: do the page. =)
                        return $element.addClass('body-wide body-err');
                    case '/signin':
                    case '/signup':
                    case '/pages/forgot-password': // TODO: do the page. =)
                        return $element.addClass('body-wide-sign body-auth');
                    case '/profile':
                    case '/dashboard':
                        return $element.addClass('body-wide body-auth');
                    case '/pages/lock-screen': // TODO: do the page. =)
                        return $element.addClass('body-wide body-lock');
                }
            };

            addBg($location.path());

            $scope.$watch(path, function(newVal, oldVal) {
                if (newVal === oldVal) {
                    return;
                }
                return addBg($location.path());
            });
        }        
    }
 
})(); 


