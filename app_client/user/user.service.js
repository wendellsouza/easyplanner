/**
 * Created by wendell on 3/13/17.
 * Created as module by wendell on 11/5/17.
 */
( function() {
    'use strict';
    function userServices ($http, authentication) {

        const dashboard = function () {
            return new Promise( function(resolve, reject) {
                $http
                    .get('/api/users/events', {
                        withCredentials: true,
                        headers: { Authorization: 'Bearer ' + authentication.getEasyToken() } } )
                    .then(function (data) {
                        resolve(data.data);
                    })
                    .catch(function (err) {
                        reject(err);
                    });
            });
        };

        const archivedEvents = function () {
            return $http.get('/api/users/events/archived', {
                headers: { Authorization: 'Bearer ' + authentication.getEasyToken() }
            });
        };

        const subscription = function (token) {
            return new Promise( function (resolve, reject) {
                $http
                    .post('/api/payment/subscription',
                        {token},
                        {headers: { Authorization: 'Bearer ' + authentication.getEasyToken() }}
                    )
                    .then(function (data) {
                        console.log(data.data);
                        authentication
                            .setLocalStorage(authentication.EASYPLANNER_TOKEN, data.data.token);
                        resolve(data);
                    })
                    .catch(function (err) {
                        reject(err);
                    });
            })
        };

        const updateCard = function (token) {
            return new Promise( function (resolve, reject) {
                $http
                    .post('/api/payment/updatecard',
                        {token},
                        {headers: { Authorization: 'Bearer ' + authentication.getEasyToken() }}
                    )
                    .then(function (stripeCustomer) {
                        resolve(stripeCustomer);
                    })
                    .catch(function (err) {
                        reject(err);
                    });
            })
        };

        const getPaymentHistory = function () {
            return new Promise( function (resolve, reject) {
                $http
                    .get('/api/payment/history',
                        {headers: { Authorization: 'Bearer ' + authentication.getEasyToken() }}
                    )
                    .then(function (history) {
                        resolve(history);
                    })
                    .catch(function (err) {
                        reject(err);
                    });
            })
        };

        const getPaymentCustomer = function () {
            return new Promise( function (resolve, reject) {
                $http
                    .get('/api/payment/customer',
                        {headers: { Authorization: 'Bearer ' + authentication.getEasyToken() }}
                    )
                    .then(function (customer) {
                        resolve(customer);
                    })
                    .catch(function (err) {
                        reject(err);
                    });
            })
        };

        const getPaymentHubId = function () {
            return new Promise( (resolve, reject)=> {
                $http
                    .get('/api/payment/getpaymenthubid',
                        {headers: { Authorization: 'Bearer ' + authentication.getEasyToken() }}
                    )
                    .then((data) => {
                        resolve(data.data.id);
                    })
                    .catch((err) => {
                        reject(err);
                    });
            });
        };

        return {
            dashboard : dashboard,
            archivedEvents : archivedEvents,
            subscription : subscription,
            getPaymentHistory : getPaymentHistory,
            getPaymentCustomer : getPaymentCustomer,
            updateCard : updateCard,
            getPaymentHubId : getPaymentHubId
        };
    }
    angular
        .module('app.user')
        .service('userServices', [ '$http', 'authentication', userServices]);
}) ();