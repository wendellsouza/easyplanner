/**
 * Created by wendell on 1/5/18.
 */
(function () {
    'use strict';

    function PaymentModalController($mdDialog, eventsService, userServices, authentication, title, $timeout, logger) {
        const vm = this;
        vm.currentUser = authentication.getCurrentUser();
        vm.title = title;

        /**
         * Custom styling can be passed to options when creating an Element.
         * @type {{base: {color: string, lineHeight: string, fontFamily: string, fontSmoothing: string, fontSize: string, "::placeholder": {color: string}}, invalid: {color: string, iconColor: string}}}
         */
        const style = {
            base: {
                color: '#32325d',
                lineHeight: '18px',
                fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                fontSmoothing: 'antialiased',
                fontSize: '16px',
                '::placeholder': {
                    color: '#b9b9b9'
                }
            },
            invalid: {
                color: '#fa755a',
                iconColor: '#fa755a'
            },
        };

        let stripeId;

        userServices.getPaymentHubId()
            .then((id) =>{
                stripeId = id;
            })
            .catch(() => {
                console.error(err);
            });

        /**
         * Create an instance of the card Element.
         * @type {cardNumber}
         */
        $timeout(() => {
            const stripe = Stripe(stripeId);
            const elements = stripe.elements();

            const cardNumberElement = elements.create('cardNumber', {
                style: style,
                placeholder: 'Número do cartão',
            });
            cardNumberElement.mount('#card-number-element');

            const cardExpiryElement = elements.create('cardExpiry', {
                style: style,
                placeholder: 'MM/AA',
            });
            cardExpiryElement.mount('#card-expiry-element');

            const cardCvcElement = elements.create('cardCvc', {
                style: style,
                placeholder: 'CVV',
            });
            cardCvcElement.mount('#card-cvc-element');

            cardNumberElement.addEventListener('change', function (event) {
                const displayError = document.getElementById('card-errors');
                if (event.error) {
                    displayError.textContent = getError(event.error);
                } else {
                    displayError.textContent = '';
                }
            });

            /** Create a token or display an error when the form is submitted. **/
            const formEditPlan = document.forms.formEditPlan;

            formEditPlan.addEventListener('submit', async (event) => {
                event.preventDefault();

                const {token, error} = await stripe.createToken(cardNumberElement, {name: vm.currentUser.name});

                if (error) {
                    // Mostar ao usuário algum erro.
                    const errorElement = document.getElementById('card-errors');
                    errorElement.textContent = getError(error);
                } else {
                    cardNumberElement.clear();
                    cardExpiryElement.clear();
                    cardCvcElement.clear();
                    // Usa o Token para Assinatura ou aleteração do Cartão.
                    stripeTokenHandler(token);
                }
            });
        }, 500);

        const getError = (error) => {
            let easyMsg;
            switch(error.message){
                case 'Your card number is incomplete.':
                    easyMsg = 'O número do cartão está incompleto.';
                    break;
                case "Your card number is invalid.":
                    easyMsg = 'O número do cartão é inválido.';
                    break;
                case "Your card's expiration date is incomplete.":
                    easyMsg = 'A data de expiração do cartão está incompleta.';
                    break;
                case "Your card's expiration year is in the past.":
                    easyMsg = 'O ano da expiração do cartão está no passado.';
                    break;
                case "Your card's expiration year is invalid.":
                    easyMsg = 'O ano da expiração do cartão é inválido.';
                    break;
                case "Your card's security code is incomplete.":
                    easyMsg = 'O código de segurança do cartão está incompleto.';
                    break;
                default:
                    easyMsg = 'Algum problema com os dados do cartão.';
                    break;
            }
            return easyMsg;
        };

        const stripeTokenHandler = (token) => {
            if (vm.currentUser.trial) {
                doSubscription(token);
            } else {
                doUpdateCard(token);
            }
        };

        const doSubscription = (token) => {
            const errorElement = document.getElementById('card-errors');
            userServices
                .subscription(token)
                .then(function (subscription) {
                    $mdDialog.hide(subscription);
                })
                .catch(function (err) {
                    console.log(err);
                    errorElement.textContent = err.data.easyplannerErr.message;
                });
        };

        const doUpdateCard = (token) => {
            userServices
                .updateCard(token)
                .then(function (stripeCustomer) {
                    vm.paymentCustomer  = stripeCustomer.data;
                    vm.currentUser.plan = vm.currentUser.trial ? 'Período de Teste' : vm.paymentCustomer.subscriptions.data[0].plan.nickname;
                    vm.collapsePaymentForm = true;
                    $mdDialog.hide(stripeCustomer);
                })
                .catch(function (err) {
                    console.log('erro: ',  err);
                    logger.logError('Erro ao alterar cartão!');
                    const errorElement = document.getElementById('card-errors');
                    errorElement.textContent = err.data.easyplannerErr.message;
                });
        };
    }

    angular
        .module('app.user')
        .controller('PaymentModalController', ['$mdDialog', 'eventsService', 'userServices', 'authentication', 'title', '$timeout', 'logger', PaymentModalController]);
})();