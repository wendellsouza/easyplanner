/**
 * Created by wendell on 1/10/18.
 */

(function () {
    'use strict';

    function NewEventController($scope, logger, $timeout, $location, eventsService, customerService) {
        const vm = this;
        vm.newEvent = {};
        vm.newEvent.date = '';

        vm.cleanObj = function () {
            vm.newEvent = {};
            vm.newEvent.date = '';
        };
        vm.canSubmit = function () {
            return $scope.formNewEvent.$valid;
        };
        vm.cancel = function () {
            vm.cleanObj();
            $scope.$emit('$newEventCancelled');
        };


        vm.defineMainCustomers = function (eventType) {
            let mainCustomers;
            switch (eventType) {
                case eventsService.eventTypesObject.ANIVERSARIO :
                case eventsService.eventTypesObject.QUINZEANOS :
                    mainCustomers = vm.newEvent.birthPerson;
                    break;
                case eventsService.eventTypesObject.CASAMENTO :
                case eventsService.eventTypesObject.BODAS :
                    mainCustomers = vm.newEvent.bride + ' e ' + vm.newEvent.groom;  // TODO: traduzir -> AND
                    break;
                default:
                    mainCustomers = vm.newEvent.customer;
            }
            return mainCustomers;
        };

        vm.insertCustomers = function (eventType) {
            return new Promise(function (resolve) {
                switch (eventType) {
                    case eventsService.eventTypesObject.ANIVERSARIO :
                    case eventsService.eventTypesObject.QUINZEANOS :
                        vm.doAddCustomer({
                            name: vm.newEvent.birthPerson,
                            lastName: vm.newEvent.birthPersonLastName,
                            role: 'Aniversariante'
                        });
                        resolve();
                        break;
                    case eventsService.eventTypesObject.CASAMENTO :
                    case eventsService.eventTypesObject.BODAS :
                        const cusArray = [
                            {name: vm.newEvent.bride, lastName: vm.newEvent.brideLastName, role: 'Noiva'},
                            {name: vm.newEvent.groom, lastName: vm.newEvent.groomLastName, role: 'Noivo'}
                        ];
                        vm.doAddCustomer(cusArray);
                        resolve();
                        break;
                    default :
                        vm.doAddCustomer({
                            name: vm.newEvent.customer,
                            lastName: vm.newEvent.customerLastName,
                            role: 'Cliente'
                        });
                        resolve();
                        break;
                }
            });
        };

        vm.doAddCustomer = function (customer) {
            const customerX = Array.isArray(customer) ? customer[0] : customer;
            customerService
                .editEventCustomer(customerX)
                .then(function () {
                    if (Array.isArray(customer)) {
                        vm.doAddCustomer(customer[1]);
                    }
                })
                .catch(function (err) {
                    console.error(err);
                    logger.logError('Erro ao criar Cliente.');
                });
        };

        vm.submitForm = function () {
            vm.newEvent.mainCustomers = vm.defineMainCustomers(vm.newEvent.type);
            vm.newEvent.date = moment(new Date(vm.newEvent.date));
            if (vm.editingTime) {
                vm.newEvent.date = moment(vm.newEvent.date).hour(parseInt(vm.editingTime.slice(0, 2))).minute(parseInt(vm.editingTime.slice(-2)));
            }
            eventsService
                .addEvent(vm.newEvent)
                .then(function () {
                    vm.insertCustomers(vm.newEvent.type)
                        .then(function () {
                            $scope.$emit('$newEventAdded');
                            $location.path('/customers');
                            /** Já seta o evento novo como ativo */
                            vm.cleanObj();
                        });
                })
                .catch(function (err) {
                    logger.logError('Erro ao criar Evento.');
                    console.error(err);
                });
        };

        /** constiáveis para md-autocomplete */
        vm.types = eventsService.EVENT_TYPES;
        vm.selectedItem = vm.newEvent.type;
        vm.simulateQuery = false;
        vm.isDisabled = false;
        vm.noCache = true;

        /** Internal methods md-autocomplete */
        function createFilterFor(query) {
            return function filterFn(type) {
                return (angular.lowercase(type).indexOf(angular.lowercase(query)) > -1);
            };
        }

        function querySearch(query) {
            return query ? vm.types.filter(createFilterFor(query)) : vm.types;
        }

        function searchTextChange(text) {
            vm.newEvent.type = text;
            show(text);
        }

        function show(item) {
            vm.showBride = [eventsService.eventTypesObject.CASAMENTO, eventsService.eventTypesObject.BODAS].indexOf(item) > -1;
            vm.showGroom = vm.showBride;
            vm.showBirthPerson = [eventsService.eventTypesObject.ANIVERSARIO, eventsService.eventTypesObject.QUINZEANOS].indexOf(item) > -1;
            vm.showClient = [eventsService.eventTypesObject.CASAMENTO,
                eventsService.eventTypesObject.BODAS,
                eventsService.eventTypesObject.ANIVERSARIO,
                eventsService.eventTypesObject.QUINZEANOS].indexOf(item) < 0;
        }

        function selectedItemChange(item) {
            vm.newEvent.type = item;
            show(item);
        }

        vm.querySearch = querySearch;
        vm.selectedItemChange = selectedItemChange;
        vm.searchTextChange = searchTextChange;
    }

    angular
        .module('app.user')
        .controller('NewEventController', ['$scope', 'logger', '$timeout', '$location', 'eventsService', 'customerService', NewEventController]);

})();