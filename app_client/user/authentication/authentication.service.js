/**
 * Created by wendell on 8/17/17.
 * Created as a module by wendell on 11/29.2017.
 */
( function () {
    'use strict';
    
    function authentication ($window, $http) {
        const EASYPLANNER_TOKEN = 'easyplanner-token'; // TODO: guardar esse nome de token em outro lugar.
        const EASYPLANNER_ACTIVE_EVENT = 'easyplanner-active-event'; // TODO: guardar esse nome de token em outro lugar.
        const EASYPLANNER_EVENT_CODE = 'easyplanner-event-code'; // TODO: guardar esse nome de token em outro lugar.

        var setLocalStorage = function (key, value) {
            $window.localStorage[key] = value;
        };
        
        var setSessionStorage = function (key, value) {
            $window.sessionStorage[key] = value;
        };

        var getLocalStorage = function (key) {
            return $window.localStorage[key];
        };
        
        var getSessionStorage = function (key) {
            return $window.sessionStorage[key];
        };

        var setEventCode = function (eventCode) {
            setSessionStorage(EASYPLANNER_EVENT_CODE, eventCode);
        };

        var getEventCode = function () {
            return getSessionStorage(EASYPLANNER_EVENT_CODE);
        };

        var isCustomerLogged = function () {
            return getEventCode() !== null;
        };

        var getEasyToken = function () {
            var token = getLocalStorage(EASYPLANNER_TOKEN);
            if (!token) {
                token = getSessionStorage(EASYPLANNER_TOKEN);
            }
            return token;
        };

        var isLoggedIn = function () {
            var token = getLocalStorage(EASYPLANNER_TOKEN);
            if (!token) {
                token = getSessionStorage(EASYPLANNER_TOKEN);
            }
            if (token) {
                var payload = JSON.parse($window.atob(token.split('.')[1] ));
                return payload.exp > Date.now() / 1000;
            } else {
                return false;
            }
        };

        var getCurrentUser = function () {
            function b64DecodeUnicode(str) {
                // Going backwards: from bytestream, to percent-encoding, to original string.
                return decodeURIComponent($window.atob(str).split('').map(function(c) {
                    return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
                }).join(''));
            }
            if (isLoggedIn()) {
                var token = getLocalStorage(EASYPLANNER_TOKEN);
                if (!token) {
                    token = getSessionStorage(EASYPLANNER_TOKEN);
                }
                var payload = JSON.parse( b64DecodeUnicode( token.split('.')[1] ) );
                return {
                    _id        : payload._id,
                    email      : payload.email,
                    name       : payload.name,
                    company    : payload.company,
                    isCustomer : payload.isCustomer,
                    trial      : payload.trial
                };
            }
        };
        
        var register = function(userCredentials) {
            userCredentials.password = $window.btoa(userCredentials.password);
            return $http.post('/api/user/register', userCredentials)
                .then( function(data) {
                    setLocalStorage(EASYPLANNER_TOKEN, data.data.token);
                });
        };

        var login = function(userCredentials) {
            var userTo;
            userTo = angular.extend({}, userCredentials);
            userTo.password = $window.btoa(userCredentials.password);
            return $http.post('/api/user/login', userTo)
                .then( function(data) {
                    if(userCredentials.keepLogged) {
                        setLocalStorage(EASYPLANNER_TOKEN, data.data.token);
                    } else {
                        setSessionStorage(EASYPLANNER_TOKEN, data.data.token);
                    }
                });
        };
        
        var logout = function() {
            $window.localStorage.removeItem(EASYPLANNER_TOKEN);
            $window.localStorage.removeItem(EASYPLANNER_ACTIVE_EVENT);
            $window.sessionStorage.removeItem(EASYPLANNER_TOKEN);
            $window.sessionStorage.removeItem(EASYPLANNER_ACTIVE_EVENT);
        };
        
        var logoutAndClose = function () {
            logout();
        };
        
        var userUpdate = function(data) {
            return $http.put('/api/user/update', data, {
                    headers: { Authorization: 'Bearer ' + getEasyToken() }})
                .then(function (data) {
                    setLocalStorage(EASYPLANNER_TOKEN, data.data.token);
                });
        };
        var invite = function (data) {
            return $http.put('/api/user/invite', data, {
                headers: { Authorization: 'Bearer ' + getEasyToken() }
            });
        };
        var checkNewUser = function (data) {
            return $http.post('/api/user/checknew', data);
        };
        var userActivate = function (userId, data) {
            return $http.post('/api/user/activate/' + userId, data);
        };
        var getUserMsgSignature = function () {
            return $http.get('/api/user/msg/signature', {
                    headers: { Authorization: 'Bearer ' + getEasyToken() }
                });
        };
        return {
            getLocalStorage : getLocalStorage,
            setLocalStorage : setLocalStorage,
            setEventCode : setEventCode,
            getEventCode : getEventCode,
            getEasyToken : getEasyToken,
            isCustomerLogged : isCustomerLogged,
            register : register,
            login : login,
            logout : logout,
            getCurrentUser : getCurrentUser,
            isLoggedIn : isLoggedIn,
            logoutAndClose : logoutAndClose,
            userUpdate : userUpdate,
            invite : invite,
            checkNewUser : checkNewUser,
            userActivate : userActivate,
            getUserMsgSignature : getUserMsgSignature,
            EASYPLANNER_TOKEN  : EASYPLANNER_TOKEN ,
            EASYPLANNER_ACTIVE_EVENT  : EASYPLANNER_ACTIVE_EVENT,
            EASYPLANNER_EVENT_CODE : EASYPLANNER_EVENT_CODE
        };
    }
    
    authentication.$inject = ['$window', '$http'];
    
    angular
        .module('app.authentication')
        .service('authentication', authentication);
    
})();


