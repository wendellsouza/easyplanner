(function () {
    'use strict';

    angular
        .module('app.authentication')
        .directive('customPage', customPage);


    // add class for specific pages to achieve fullscreen, custom background etc.
    function customPage() {
        var directive = {
            restrict: 'A',
            controller: ['$scope', '$element', '$location', customPageCtrl]
        };

        return directive;

        function customPageCtrl($scope, $element, $location) {
            var addBg, path;

            path = function() {
                return $location.path();
            };

            addBg = function(path) {
                $element.removeClass('body-wide body-wide-sign body-err body-lock body-auth body-outside');

                switch (path) {
                    case '/signin':
                    case '/signup':
                        return $element.addClass('body-wide-sign body-auth');
                    case '/dashboard':
                    case '/profile':
                        $element.addClass('body-wide');
                        $element.removeClass('on-canvas'); // Garante que não aparecerá o sidebar
                        break;
                    case '/updateRsvp':
                    case '/confirmGuest':
                    case '/rsvp':
                        $element.addClass('body-wide body-outside');
                        break;
                }
                if ( path.includes('/rsvpnoivos') ) { // essa url tem o parâmetro do código de acesso
                    $element.addClass('body-wide body-outside');
                }
            };

            addBg($location.path());

            $scope.$watch(path, function(newVal, oldVal) {
                if (newVal === oldVal) {
                    return;
                }
                return addBg($location.path());
            });
        }
    }

})();