/**
 * Created by wendell on 12/5/17.
 */
(function () {
    'use strict';
    function AuthenticationController ($scope, $window, $location, logger, authentication, $timeout) {

        /**
         * Se usuário estiver logado e somente nas telas de sigun e signup direciona para o dashboard
         * */
        if (['/signin', '/signup'].includes($location.$$path) && authentication.isLoggedIn()) {
            $location.path('/dashboard');
        }

        $timeout(function () {angular.element('#loginEmail').focus(); }, 100);

        $scope.doLogin = function () {
            authentication
                .login($scope.login)
                .then(function () {
                    $scope.login = {};
                    $location.path('/dashboard');
                }, function (err) {
                    logger.logError(err.data.message);
                    console.log(err);
                });
        };

        $scope.logout = function () {
            authentication.logoutAndClose();
        };
        
        $scope.signup = function () {
            $location.url('/');
        };
        
        $scope.reset = function () {
            $location.url('/');
        };
        
        $scope.unlock = function () {
            $location.url('/');
        };
    }
    
    function SignupController ($location, authentication, logger, $timeout) {
        var vm = this;
        vm.returnPage = $location.search().page || '/';
        vm.onSubmit = function() {
            if (!vm.newUser || !vm.newUser.name || !vm.newUser.email || !vm.newUser.password || !vm.newUser.confirmPassword) {
                logger.logWarning('Informe todos os campos obrigatórios, por favor.');
                return false;
            } else if (vm.newUser.password !== vm.newUser.confirmPassword) {
                logger.logWarning('A confirmação da senha está diferente da senha!');
                return false;
            }
            else {
                vm.newUser.confirmPassword = undefined;
                vm.doSignUser(vm.newUser);
            }
        };

        vm.doSignUser = function(newUser) {
            authentication
                .register(newUser)
                .then(function() {
                    logger.logSuccess('Cadastrado feito com sucesso!');
                    $timeout( function() {$location.path('/dashboard');}, 1000);
                }, function(err) {
                    console.log(err);
                    console.log(err.data);
                    if (err.data.code === '11000' || err.data.errmsg.indexOf('E11000 duplicate key error') > -1) {
                        logger.logWarning('E-mail já cadastrado!');
                    } else {
                        console.log(err);
                    }
                });
        };
    }
    
    angular
        .module('app.authentication')
        .controller('AuthenticationController', ['$scope', '$window', '$location', 'logger', 'authentication', '$timeout', AuthenticationController])
        .controller('SignupController', ['$location', 'authentication', 'logger', '$timeout', SignupController]);
    
})();