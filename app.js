require('dotenv').load();
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

/* Carregar Passport antes da definição dos modelos do BD */
var passport = require('passport');

require('./app_api/models/db');
var ctrlApp = require('./app_api/controllers/app'); // Usa mongoose models e deve ficar depois dos models

/* Carregar estratégia(Srategy) depois das definições de modelo do BD, pois ele precisa do modelo de usuário(User) */
require('./app_api/config/passport.js');

var reoutesApi   = require('./app_api/routes/index');
var routesEvents = require('./app_api/routes/events');

var app = express();
function requireHTTPS(req, res, next) {
    if (process.env.NODE_ENV !== "development" && !req.secure && req.get('x-forwarded-proto') !== 'https') {
        return res.redirect('https://' + req.get('host') + req.url);
    }
    next();
}

/**
 * Redireciona para HTTPS
 * */
app.use(requireHTTPS);

// uncomment after placing your favicon in /public
app.use(favicon(path.join(__dirname, 'app_client', 'favicon.gif')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

/* Estatics routes  */
app.use(express.static(path.join(__dirname, 'app_client')));
app.use('/api', routesEvents);


/**
 * Passport deve ser iniciado depois das rotas estáticas e antes das rotas de autentição
 * */
app.use( passport.initialize() );

/* Authentication Routes */
app.use('/api', reoutesApi);

/**
 *     PROVÊ O FRONT-END
 * */
app.use( function(req, res) {
    res.sendFile(path.join(__dirname, 'app_client', 'index.html'));
});


/**
 * Scheduled tasks: task Reminders
 * */
// var cron = require('node-cron');
// cron.schedule('* * * 1 * *', function(){
//     ctrlApp.getTasksRemainders();
//     console.log('running a task every 1 day.');
// });


// catch 404 and forward to error handler
// app.use(function(req, res, next) {
//     var err = new Error('Not Found! :(');
//     err.status = 404;
//     next(err);
// });

/**
 * Errors handlers
 * */

/** Cath unauthorized errors */
app.use(function (err, req, res, next) {
    console.log(err);
    if(err.name === 'UnauthorizedError') {
        res.status(401);
        res.json( { "message" : err.name + ": " + err.message + " - Usuário não autorizado!" } );
    }
});

/** General errors */
// app.use(function(err, req, res) {
//     // set locals, only providing error in development
//     res.locals.message = err.message;
//
//     res.locals.error = req.app.get('env') === 'development' ? err : {};
//     // render the error page
//     res.status(err.status || 500);
//     res.render('error');
// });

// TODO: não está pegando páginas não encontradas!!

app.use(function(req, res, next){
    res.status(404);

    // respond with html page
    if (req.accepts('html')) {
        res.render('404', { url: req.url });
        return;
    }

    // respond with json
    if (req.accepts('json')) {
        res.send({ error: 'Not found' });
        return;
    }

    // default to plain-text. send()
    res.type('txt').send('Not found');
});

module.exports = app;
